var express = require('express');
var router = express.Router();
//var fx = require("money");


const AWS = require('aws-sdk');
const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');



AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
//const dataTransfer = require('../app/utilities/invoice');

// ##############################################################################  

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-1 ##########
let mcube_transcode_BASE = API.mcube_transcode.base.development;

// ########## MICRO-2 ##########

let mcube_adv_transcode_BASE = API.mcube_adv_transcode.base.development;



router.get('/', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('invoice/index', { section:'invoice', sub_section: '', userData: userData, userName: userName, userRole: userRole });

})

function getTranscodedAssets(tableName) {
    return new Promise((resolve, reject) => {

        var params = {
            TableName: tableName
        };

        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.scan(params, onScan);
        function onScan(err, data) {
            if (err) {
                //console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                //console.log(`\n \n ${tableName} >> Scan succeeded. ${JSON.stringify(data)}`);
                resolve(data)
            }
        }
    })
}

router.get('/exchangeratesapi', (req, res, next) => {

    let params = {
        url: `https://api.exchangeratesapi.io/latest`
    }

    requestMethods.getApiRequest(params).then((exchangerates) => {
        res.json(exchangerates);
    });
})

router.post('/getProfiles', (req, res, next) => {
    console.log("getProfiles post called in routes for invoice >> ")


    let tenantId = req.session.tenantId;
    
    console.log("getProfiles post called in routes for invoice tenantId >> " +JSON.stringify(tenantId))


    let previewTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.previewTbl
    let basicProfileTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.basicProfileTbl
    let standardProfileTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.standardProfileTbl
    let premiumProfileTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.premiumProfileTbl

    const promise1 = getTranscodedAssets(previewTbl);
    const promise2 = getTranscodedAssets(basicProfileTbl);
    const promise3 = getTranscodedAssets(standardProfileTbl);
    const promise4 = getTranscodedAssets(premiumProfileTbl);

    Promise.all([promise1, promise2, promise3, promise4]).then(function (response) {
        //console.log(`\n\n\n\n\n\ response  >> ${JSON.stringify(response)}`);

        res.json({
            status: 200,
            message: 'getProfiles success.',
            data: response,
        });

    })

})


module.exports = router;  