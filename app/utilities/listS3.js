var AWS = require('aws-sdk');
const util = require('util');
// eslint-disable-next-line no-undef

const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
             
const s3 = new AWS.S3();

//////// /////// ################ %% SNS TRIGGERERING %% ################    ///////////

// To insert into DynamoDB
exports.updateDynamodb = (reqParams) => {

    console.log("In common Request method  updatedynamodb in lists3 in mcube ui >> >> " +JSON.stringify(reqParams))

    return new Promise((resolve, reject) => {

        var docClient = new AWS.DynamoDB.DocumentClient()

        var params = {
            TableName: reqParams.TableName,
            Key: {
                'assetid': reqParams.assetid
            },
            UpdateExpression: "set audio_info = :a",
            ExpressionAttributeValues: {
                ":a": reqParams.audio_info
            },
            ReturnValues: "UPDATED_NEW"
        }

        docClient.update(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else { 
                // console.log("updateDynamodbPromise succeeded for Update Audio status:." + JSON.stringify(data));
                resolve()
            }
        });

    })
};


exports.putItemIntoDyanamodb = (reqParams) => {

    console.log("In common Request method  updatedynamodb in lists3 in mcube ui >> >> " +JSON.stringify(reqParams))

    return new Promise((resolve, reject) => {
        var dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
        //var docClient = new AWS.DynamoDB.DocumentClient()
        var params = {
            Item: {
             "cilentName": {
               S: reqParams.tenantName
              }, 
             "clientId": {
               S: reqParams.clientId
              }, 
             "datePaid": {
               S: reqParams.date
              }, 
              "CardDetails": {
                S: reqParams.bankName
               },
               "Amount": {
                S: reqParams.ammount
               },
               "status": {
                S: "paid"
               },
               "AmountMonth": {
                S: reqParams.AmountMonth
               },
               

            }, 
            ReturnConsumedCapacity: "TOTAL", 
            TableName: "qa-dam-tenantInvoice"
           };
           console.log("updateDynamodbPromise parms>> ." + JSON.stringify(params));
           dynamodb.putItem(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else { 
                 console.log("updateDynamodbPromise succeeded for Update Audio status:." + JSON.stringify(data));
                resolve()
            }
        });

    })
};


exports.getItemIntoDyanamodb = (reqParams) => {
    return new Promise((resolve, reject) => {
        var dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
var params = {
    Key: {
     "cilentName": {
       S: reqParams.clientName
      }
    }, 
    TableName: "qa-dam-tenantInvoice"
   };
   console.log("\n getItemIntoDyanamodb "+JSON.stringify(params))
   dynamodb.getItem(params, function(err, data) {
     if (err) {
         console.log(err, err.stack);
      } // an error occurred
     else {
        
        console.log(data);
        resolve(data)
     }   
   }) 
   })
   }