// Load the SDK for JavaScript
var AWS = require('aws-sdk');
const awsConfig = require('../../config/aws_config')['development'];
const video_utility = require('../../app/utilities/video');
const videoPartial = require('../../app/utilities/video/index');
const Video = require('../../workers/video');
const videoObj = new Video.VideoClass();
AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

// To read getDynamodbStatus
exports.getDynamodbStatus = (reqParams) =>{
    console.log("getDynamodbStatus called with params > " + JSON.stringify(reqParams))
    return new Promise((resolve, reject) => {
        var params = {
            TableName: reqParams.tableName,
            Key: {
                'assetid': reqParams.contentId,
            }
        };

        let CloudFront = reqParams.CloudFront_URL ? reqParams.CloudFront_URL : null;
        //console.log("AWS CONFIG ## > ", JSON.stringify(AWS.config))
        // ************************** //
        // FIX FOR ERROR IN READING ITEM FROM DYNAMODB
        // Unable to read item. Error ForbiddenException: Forbidden
        delete AWS.config['endpoint']; // ISSUE IS MC
        // ************************** //
        var docClient = new AWS.DynamoDB.DocumentClient();
        const statusJobPromise = docClient.get(params).promise();
        statusJobPromise.then(
            (data) => {
                console.log("GetItem from DynamoDB succeeded : > " + JSON.stringify(data));
                if (data.Item && (data.Item.status === 'COMPLETE' || data.Item.status === 'ERROR')) {
                    let _params;
                    switch (reqParams.transcode_type) {
                        case 'preview':
                            var preview_url= data.Item.preview_url ? data.Item.preview_url : null;
                            var partsOF_preview_url = preview_url.split("/");
                            var previewCloudFront_Url = `${partsOF_preview_url[0]}//${CloudFront}/${partsOF_preview_url[3]}/${partsOF_preview_url[4]}/${partsOF_preview_url[5]}/${partsOF_preview_url[6]}/${partsOF_preview_url[7]}`
                           console.log("\n previewCloudFront_Url >> "+JSON.stringify(previewCloudFront_Url))
                            _params = {
                                mongoDbName: reqParams.mongoDbName,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                media_job_status: data.Item.status ? data.Item.status : null,
                                media_job_id: data.Item.job_id_mc ? data.Item.job_id_mc : null,
                                previewCloudFront_Url: previewCloudFront_Url,
                                video_path: data.Item.video_path ? data.Item.video_path : null,
                               preview_url: data.Item.preview_url ? data.Item.preview_url : null,
                            }
                            // get thumbnails and update
                            let getThumbnailParams = {
                                media_job_status: data.Item.status ? data.Item.status : null,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                bucket : reqParams.bucket
                            }
                            videoPartial.listS3AndGetThumbnail(getThumbnailParams).then((thumbnails) => {
                                const updateParamsThumbnail = {
                                    mongoDbName: reqParams.mongoDbName,
                                    content_id: reqParams.contentId,
                                    thumbnail: (thumbnails || []),
                                    thumbnail_pic: (data.Item.status === 'COMPLETE' && thumbnails.length < 3 ? thumbnails[0] : (thumbnails[2] || ''))
                                };
                                videoObj.findOneAndUpdate(updateParamsThumbnail).then((rs) => {
                                    console.log('VIDEO THUMBNAILS UPDATE COMPLETE!!!!');
                                });
                            });
                            break;
                        case 'qa-dam-basic-db':

                            var basic_transcode_url = data.Item.playback_url ? data.Item.playback_url : null;
                            var partsOF_basic_url = basic_transcode_url.split("/");
                            var basicCloudFront_transcode_Url = `${partsOF_basic_url[0]}//${CloudFront}/${partsOF_basic_url[3]}/${partsOF_basic_url[4]}/${partsOF_basic_url[5]}/${partsOF_basic_url[6]}/${partsOF_basic_url[7]}`
                           
                            _params = {
                                mongoDbName: reqParams.mongoDbName,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                basic_transcode_status: data.Item.status ? data.Item.status : null,
                                basicCloudFront_transcode_Url: basicCloudFront_transcode_Url ? basicCloudFront_transcode_Url: null,
                            }
                            break;
                        case 'qa-dam-standard-db':
                            var standard_transcode_url = data.Item.playback_url ? data.Item.playback_url : null;
                            var partsOF_standard_url = standard_transcode_url.split("/");
                            var standardCloudFront_transcode_Url = `${partsOF_standard_url[0]}//${CloudFront}/${partsOF_standard_url[3]}/${partsOF_standard_url[4]}/${partsOF_standard_url[5]}/${partsOF_standard_url[6]}/${partsOF_standard_url[7]}`
                           
                            _params = {
                                mongoDbName: reqParams.mongoDbName,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                standard_transcode_status: data.Item.status ? data.Item.status : null,
                                standardCloudFront_transcode_Url: standardCloudFront_transcode_Url ? standardCloudFront_transcode_Url : null,
                            }
                            break;
                        case 'qa-dam-premium-db':

                            var premium_transcode_url = data.Item.playback_url ? data.Item.playback_url : null;
                            var partsOF_premium_url = premium_transcode_url.split("/");
                            var premiumCloudFront_transcode_Url = `${partsOF_premium_url[0]}//${CloudFront}/${partsOF_premium_url[3]}/${partsOF_premium_url[4]}/${partsOF_premium_url[5]}/${partsOF_premium_url[6]}/${partsOF_premium_url[7]}`

                            _params = {
                                mongoDbName: reqParams.mongoDbName,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                premium_transcode_status: data.Item.status ? data.Item.status : null,
                                premiumCloudFront_transcode_Url: premiumCloudFront_transcode_Url ? premiumCloudFront_transcode_Url : null,
                            }
                            break;
                        case 'qa-dam-custom-db':

                            var custom_transcode_url=  data.Item.playback_url ? data.Item.playback_url : null;
                            var partsOF_custom_url = custom_transcode_url.split("/");
                            var customCloudFront_transcode_Url = `${partsOF_custom_url[0]}//${CloudFront}/${partsOF_custom_url[3]}/${partsOF_custom_url[4]}/${partsOF_custom_url[5]}/${partsOF_custom_url[6]}/${partsOF_custom_url[7]}`
    
                            _params = {
                                mongoDbName: reqParams.mongoDbName,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                custom_transcode_status: data.Item.status ? data.Item.status : null,
                                customCloudFront_transcode_Url: customCloudFront_transcode_Url ? customCloudFront_transcode_Url : null,
                            }
                    }
                    videoObj.findOneAndUpdate(_params).then((rs) => {
                        console.log('VIDEO UPDATE COMPLETE!!!!');
                    });
                } else {
                    setTimeout(() => {
                        video_utility.getDynamodbStatus(reqParams);
                    }, 2000);
                }
                // resolve(data)
            },
            (err) => {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err.stack, null, 2));
                // reject(err)
            }
        );
    })
};
// To insert into DynamoDB
exports.updateDynamodb = (reqParams) => {
    return new Promise((resolve, reject) => {
        var dynamodb = new AWS.DynamoDB();
        var params = {
            Item: {
                'assetid': { S: reqParams.contentId },
                'job_id_mc': { S: reqParams.jobId },
                'playback_url': { S: reqParams.playback_url },
                'status': { S: 'PROGRESSING' }
            },
            TableName: awsConfig.DYNAMO_DB.tables.custom_transcode
        };
        //console.log("updateDynamodb called with params > " + JSON.stringify(params))
        dynamodb.putItem(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else {
                console.log("updateDynamodbPromise succeeded:." + JSON.stringify(data));
                resolve()
            }
        });
    })
};
//////// /////// ################ %% SNS TRIGGERERING %% ################    ///////////
// Video transcode_asset
exports.transcode_asset = reqParams => new Promise((resolve, reject) => {
    console.log()
    var sns = new AWS.SNS();
    var profiles = reqParams.profiles;
    var TopicArn;
    profiles.forEach((element, i) => {
        switch (profiles[i]) {
            case 'basic':
                TopicArn = awsConfig.SNS_CONFIG.TopicArn.basic;
                break;
            case 'standard':
                TopicArn = awsConfig.SNS_CONFIG.TopicArn.standard;
                break;
            case 'premium':
                TopicArn = awsConfig.SNS_CONFIG.TopicArn.premium;
        }
        var sns_params = {
            Message: JSON.stringify({ content_path: reqParams.content_path }),
            TopicArn: TopicArn
        };
        // console.log("sns params >> " + JSON.stringify(sns_params))
        sns.publish(sns_params, function (err, data) {
            if (err) {
                console.log("transcode_asset Publish ERROR >", err, err.stack); // an error occurred
                reject(err)
            } else {
                console.log("transcode_asset Publish SUCCESS for > ");
            }
        });
        if (i == profiles.length - 1) {
            resolve("All SNS Published Successfully!")
        }
    });
})