const axios = require('axios');

exports.getApiRequest = reqParams => new Promise((resolve, reject) => {
    console.log("\n getApiRequest params >> " + JSON.stringify(reqParams))
    axios.get(reqParams.url)
        .then((response) => {
            console.log("\n success in getApiRequest")
            resolve(response.data) // handle success
        })
        .catch((error) => {
            console.log("\n ERROR in getApiRequest > " + error)
            reject() // handle error
        })
        .finally(() => {
            // always executed
        });
})

exports.getApiRequestWithHeaders = reqParams => new Promise((resolve, reject) => {
    console.log("\n getApiRequestWithHeaders params >> " + JSON.stringify(reqParams))
    axios.get(reqParams.url, { params: {}, headers: reqParams.headers })
        .then(function (response) {
            console.log("\n success in getApiRequestWithHeaders")
            resolve(response.data) // handle success
        })
        .catch(function (error) {
            console.log("\n ERROR in getApiRequestWithHeaders > " + error)
            reject() //handle error
        })
        .finally(function () {
            //always executed
        });
})


exports.postApiRequest = reqParams => new Promise((resolve, reject) => {
    console.log("\n postApiRequest params >> " + JSON.stringify(reqParams))
    axios.post(reqParams.url,
        reqParams.data
    )
        .then(function (response) {
            console.log("\n success in postApiRequest")
            resolve(response.data) // handle success
        })
        .catch(function (error) {
            console.log("\n ERROR in postApiRequest > " + error)
            reject() // handle error
        })
        .finally(function () {
            // always executed
        });
})

exports.postApiRequestWithHeader = reqParams => new Promise((resolve, reject) => {
    axios.post(reqParams.url, reqParams.data, { headers: reqParams.headers })
        .then(function (response) {
            console.log("\n success in postApiRequestWithHeader >>")
            resolve(response.data) // handle success
            console.log(JSON.stringify(response.data))
        })
        .catch(function (error) {
            console.log("\n ERROR in postApiRequestWithHeader > " + error)
            reject(error) // handle error
        })
        .finally(function () {
            // always executed
        });
})


exports.deleteApiRequestWithHeader = reqParams => new Promise((resolve, reject) => {
    console.log("\n deleteApiRequestWithHeader params >> " + JSON.stringify(reqParams))
    axios.delete(reqParams.url, { data: reqParams.data, headers: reqParams.headers })
        .then(function (response) {
            console.log("\n success in deleteApiRequestWithHeader")
            resolve(response.data) // handle success
        })
        .catch(function (error) {
            console.log("\n ERROR in deleteApiRequestWithHeader > " + error)
            reject() // handle error
        })
        .finally(function () {
            // always executed
        });
})