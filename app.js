const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const chalk = require('chalk');
const session = require('express-session');
const mongoose = require('mongoose');
const helmet = require('helmet');
const log4js = require('log4js');
const logger = log4js.getLogger();
const cors = require('cors');
const compression = require('compression');

const mongoDb = require('./config/mongoDb');

// connect to mongoDB
mongoDb.getMongoDbs().then((dbs) => {
  for (let db of dbs) {
    mongoDb.connectMongoDb(db.name)
  }
})


// ****************** SETTING GLOBAL ENV ******************
global.ENV = process.env.NODE_ENV || 'development';
console.log('APP JS ENVIRONMENT: ', ENV);

const index = require('./routes/index');
const users = require('./routes/users');
const video = require('./routes/video');
const dashboard = require('./routes/dashboard');
const profile = require('./routes/profile');
const settings = require('./routes/settings');
const imdb = require('./routes/testimdb');
const admin = require('./routes/admin');
const config = require('./routes/config');
const invoice = require('./routes/invoice');
const paytm = require('./routes/paytm');

const app = express();

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// helmet for security purpose
app.use(helmet());

// Logging Http Request
app.use(log4js.connectLogger(logger));

// CORS - To hanlde cross origin requests
app.use(cors());

// Parsing the body of the http
app.use((req, res, next) => {
  bodyParser.json({
    limit: '26000mb',
    verify: (req, res, buf, encoding) => {
      req.rawBody = buf.toString();
    }
  })(req, res, err => {
    if (err) {
      res.status(400).send('Bad body Request');
      return;
    }
    next();
  });
});

// to support URL-encoded bodies
app.use(bodyParser.urlencoded({
  limit: '26000mb',
  extended: true
}));

app.use(cookieParser());

//compress all responses to Make App Faster
app.use(compression());

app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: 'live-trim-session-parameter',
  resave: true,
  saveUninitialized: true
}));

app.use('/v1', router);
app.use('/', index);
app.use('/testimdb', imdb);
app.use('/config', config);

app.use((req, res, next) => {
  console.log(req.session.user_id);
  if (req.session.user_id == null) {
    // if user is not logged-in redirect back to login page //
    res.redirect('/');
  } else {
    next();
  }
});


app.use('/v1/users', users);
app.use('/v1/video', video);
app.use('/v1/dashboard', dashboard);
app.use('/v1/profile', profile);
app.use('/v1/settings', settings);
app.use('/v1/admin', admin);
app.use('/v1/invoice', invoice);
app.use('/v1/paytm', paytm);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});


mongoose.set('debug', true) // for dev only


module.exports = app;