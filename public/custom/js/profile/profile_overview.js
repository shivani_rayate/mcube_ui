
// document.ready
$(function () {

    // get all profiles
    $.get('/v1/profile/getAllProfiles',
        function (data, status) {
            if (data.status == 200) {
                appendProfilesDatatable(data);
            }
        });

})


function appendProfilesDatatable(data) {
    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var _id = element._id ? element._id : "";
        var profile_name = element.profile_name ? element.profile_name : "";
        var created_at = element.created_at ? moment(element.created_at).format('lll') : "";
        var created_by = element.created_by ? element.created_by : "Admin";

        options_table += '<tr class="profile-overview-tbl-row profile-row" id=' + _id + ' data-profile_name="' + profile_name + '">' +
            '<td class="profile-title">' + profile_name + '</td>' +
            '<td class="profile-title"> ' + created_at + '</td>' +
            '<td class="profile-title"> ' + created_by + '</td>' +
            '<td class="action-td" id=' + _id + '> <ul class="list-inline mb-0"> <li class="list-inline-item delete-profile"  title="delete" > <a class="icon circle-icon small red"><i class="mdi mdi-close text-danger"></i></a> </li> </ul> </td>';

        if (i == array.length - 1) {
            $('#list_all_profile_tbody').append(options_table)
            reInitializeDataTable()
        }
    })
}


var list_all_profile_table;

function reInitializeDataTable() {
    $("#list_all_profile_table").DataTable().destroy()
    list_all_profile_table = $('#list_all_profile_table').DataTable({
    })

}


function appendProfileData(profile_data) {

    return new Promise((resolve, reject) => {

        var options_table = "";
        profile_data.forEach(function (element, i) {

            // var _id = element._id ? element._id : "";
            var height = element.height ? element.height : "";
            var width = element.width ? element.width : "";
            var bitrate = element.bitrate ? element.bitrate : "";
            var frame = element.frame ? element.frame : "";
            var gop = element.gop ? element.gop : "";
            var audio_bitrate = element.audio_bitrate ? element.audio_bitrate : "";


            options_table += `<tr id="${i + 1}">
                                <th>${i + 1}</th>
                                <td>${height}</td>
                                <td>${width}</td>
                                <td>${bitrate}</td>
                                <td>${frame}</td>
                                <td>${gop}</td>
                                <td>${audio_bitrate}</td>
                            </tr>`;

        })

        profile_table = `<table class="table mb-0" id="profile-editable-table"> <thead class="thead-multicolour" > <tr> <th>#</th><th>Height</th> <th>Width</th> <th>Bitrate</th> <th>Frame</th> <th>GOP</th> <th>Audio Bitrate</th> </tr> </thead> <tbody id="list_all_profile_resolution">${options_table}</tbody> </table>`;

        resolve(profile_table)

    })
}


function getMetaData(profile_name) {

    return new Promise((resolve, reject) => {
        $.post('/v1/profile/preload_profiledata',
            {
                profile_name: profile_name,
            },
            function (data, status) {
                if (data.status === 200) {

                    let _data = data.data.profiles;

                    resolve(_data)
                } else {
                    toastr.error(data.message);
                    reject()
                }
            })
    });
}


// profile Selection
$(document).on("click", ".profile-overview-tbl-row", function (e) {

    $('.profile-overview-tbl-row').removeClass("active");
    $(this).addClass("active");

    // ########## child rows code ########### //
    var tr = $(this).closest('tr');
    var row = list_all_profile_table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    } else {

        let profile_name = $(".profile-overview-tbl-row.active").data("profile_name");

        //preload metadata
        getMetaData(profile_name).then((profile_data) => {

            // to close the child of row which is already open
            if (list_all_profile_table.row('.shown').length) {
                var _tr = $('tr.shown');
                var _row = list_all_profile_table.row(_tr);
                _row.child.hide();
                _tr.removeClass('shown');
            }

            appendProfileData(profile_data).then((formattedProfile_data) => {
                // Open this row
                row.child(formattedProfile_data).show();
                tr.addClass('shown');

                $('#profile-editable-table').Tabledit({
                    buttons: {
                        edit: {
                            class: "btn btn-success",
                            html: '<span class="mdi mdi-pencil"></span>',
                            action: "edit"
                        }
                    },
                    inputClass: "form-control form-control-sm",
                    deleteButton: !1,
                    saveButton: !1,
                    autoFocus: !1,
                    columns: {
                        identifier: [0, "id"],
                        editable: [
                            [1, "Height"],
                            [2, "Width"],
                            [3, "Bitrate"],
                            [4, "Frame"],
                            [5, "GOP"],
                            [6, "Audio Bitrate"]
                        ]
                    }
                })

            })

        })

    }

});



// Delete Profile 
$(document).on("click", ".delete-profile", function (e) {


    var _id = $(this).closest('tr').attr('id');


    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {

        if (t.value) {

            $.ajax({
                url: `/v1/profile/deleteProfile/${_id}`,
                type: 'DELETE',
                headers: {
                    'Content-Type': "application/json",
                },
                'success': function (data, textStatus, request) {
                    if (data.status === 200) {
                        toastr.success(data.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);

                    } else {
                        toastr.error(data.message);
                    }
                },
                'error': function (request, textStatus, errorThrown) {
                    console.log("ERR " + errorThrown)
                }
            })
        }

    })

    e.preventDefault()
})