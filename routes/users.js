var express = require('express');
var router = express.Router();

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-3 ##########
let mcube_cognito_BASE = API.mcube_cognito.base.development;


/* GET users listing. */
router.get('/', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/index', { section: 'User Management', sub_section: '', userData: userData, userName: userName, userRole: userRole });
});


router.get('/user-groups', function (req, res, next) {
  const userData = req.session.userData ? req.session.userData : null;
  // var params=userData.accessToken.jwtToken;
  // console.log("params data > "+JSON.stringify(params))
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('user/user-groups', { section: 'Users Listing', sub_section: '', userData: userData, userName: userName, userRole: userRole });
})


router.get('/register', function (req, res, next) {
  res.render('user/register');
});



router.get('/reset-pass', function (req, res, next) {
  res.render('user/reset_pass');
});


// confirmSignUp
router.post('/confirmSignUp', function (req, res, next) {
  console.log("confirmSignUp POST called")
  const params = {
    ConfirmationCode: req.body.ConfirmationCode ? req.body.ConfirmationCode : null,
    Username: req.body.Username ? req.body.Username : null,
  }
  cognito.confirmSignUp(params).then((response) => {
    if (response) {
      console.log('confirmSignUp response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Signup Confirmed',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Signup Confirmation: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
});


// registration
router.post('/register', (req, res, next) => {

  console.log("register POST called in UI")

  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.register,
    data: {
      name: req.body.name ? req.body.name : null,
      email: req.body.email ? req.body.email : null,
      password: req.body.password ? req.body.password : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  }

  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


// group creation
router.post('/createGroup', (req, res, next) => {

  console.log("createGroup POST called")

  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.createGroup,
    data: {
      groupname: req.body.groupname ? req.body.groupname : null,
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };

  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN GROUP CREATION: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


// Get All Users
router.get('/getUsers', (req, res, next) => {
  console.log('\n /getUsers called in mcube_ui');

  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.getUsers,
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  }

  requestMethods.getApiRequestWithHeaders(params).then((users) => {
    res.json(users);
  }).catch((err) => {
    console.log(`error IN getUsers: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


// Delete User
router.post('/deleteUser', (req, res, next) => {
  console.log("deleteUser POST called")
  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.deleteUser,
    data: {
      username: req.body.username ? req.body.username : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN USER Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// getUserInfo
router.get('/getUserInfo/:username', (req, res, next) => {
  console.log('***getUserInfo CALLED***');

  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.getUserInfo + req.params.username,
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };

  requestMethods.getApiRequestWithHeaders(params).then((user) => {
    res.json(user);
  })
})


// disableUser
router.post('/disableUser', (req, res, next) => {
  console.log("disableUser POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.disableUser,
    data: {
      username: req.body.username ? req.body.username : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN USER Disable: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// enableUser
router.post('/enableUser', (req, res, next) => {
  console.log("enableUser POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.enableUser,
    data: {
      username: req.body.username ? req.body.username : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN USER Enable: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// Change Password
router.post('/changePassword', (req, res, next) => {
  console.log("ChangePassword POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.changePassword,
    data: {
      accesstoken: req.session.userData ? req.session.userData.accessToken.jwtToken : null,
      currentpassword: req.body.currentpassword ? req.body.currentpassword : null,
      newpassword: req.body.newpassword ? req.body.newpassword : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  }
  console.log("change password in cognito > " + JSON.stringify(params))
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    if (response.status === 200) {
      setTimeout(function () {
        req.session.destroy(function (err) {
          if (err) {
            res.redirect('/login');
          } else {
            req.session = null;
          }
        });
      }, 3000)
      res.json(response);
    } else {
      res.json(response);
    }

  }).catch((err) => {
    console.log(`error in Password: ${err}`);
    res.json({
      status: err.status,
      message: err.message,
      data: err.data,
    })
  });
})


// Update User Attributes
router.post('/updateUserAttributes', (req, res, next) => {
  console.log("Update User Attribute  POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.updateUserAttributes,
    data: {
      accesstoken: req.session.userData ? req.session.userData.accessToken.jwtToken : null,
      UserAttributes: req.body.UserAttributes ? req.body.UserAttributes : null,
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  }
  console.log("update user attributes > " + JSON.stringify(params))
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN USER: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

})


// add User To Group
router.post('/addUserToGroup', (req, res, next) => {
  console.log("addUserToGroup POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.addUserToGroup,
    data: {
      username: req.body.username ? req.body.username : null,
      groupname: req.body.groupname ? req.body.groupname : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };

  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN addUserToGroup: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})





// Create Group
router.post('/createGroup', (req, res, next) => {
  console.log("createGroup POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.createGroup,
    data: {
      groupname: req.body.groupname ? req.body.groupname : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }

  };

  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN Group Creation: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// Get All Groups
router.get('/getListOfGroups', (req, res, next) => {
  console.log('***GET getListOfGroups CALLED***');

  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.getListOfGroups,
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  }

  requestMethods.getApiRequestWithHeaders(params).then((response) => {
    res.json(response);
  })

});



// Delete User
router.post('/deleteGroup', (req, res, next) => {
  console.log("deleteGroup POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.deleteGroup,
    data: {
      groupname: req.body.groupname ? req.body.groupname : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };

  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN Group Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})



// getGroupInfo
router.get('/getGroupInfo/:groupname', (req, res, next) => {
  console.log('***getGroupInfo CALLED***');

  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.getGroupInfo + req.params.groupname,
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };
  requestMethods.getApiRequestWithHeaders(params).then((group) => {
    res.json(group);
  })

})



// Get List Users In Group
router.get('/listUsersInGroup/:groupname', (req, res, next) => {
  console.log('***GET ListUsersInGroup CALLED***');

  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.listUsersInGroup + req.params.groupname,
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  }

  //console.log('***GET ListUsersInGroup CALLED***' + JSON.stringify(params))

  requestMethods.getApiRequestWithHeaders(params).then((users) => {
    res.json(users);
  })

});

router.get('/settings', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;

  res.render('user/settings', { section: 'User Management', sub_section: 'Settings', userData: userData, userName: userName, userRole: userRole });

});





// Delete User
router.post('/adminRemoveUserFromGroup', (req, res, next) => {
  console.log("adminRemoveUserFromGroup POST called")
  const params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.adminRemoveUserFromGroup,
    data: {
      groupname: req.body.groupname ? req.body.groupname : null,
      username: req.body.username ? req.body.username : null
    },
    headers: {
      token: req.session.userData.idToken.jwtToken
    }
  };

  //console.log("routes admin remove user group > " + JSON.stringify(params))
  requestMethods.postApiRequestWithHeader(params).then((response) => {
    res.json(response);
  }).catch((err) => {
    console.log(`error IN User Removed From Group: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

module.exports = router;