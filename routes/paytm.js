var express = require('express');
var router = express.Router();

const { initPayment, responsePayment } = require("../app/utilities/paytm/services/index");
const paytmConfig = require("../app/utilities/paytm/config");
const { param } = require('./video');

const listS3 = require('../app/utilities/listS3');

const AWS = require('aws-sdk');
const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');



AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('paytm/index', { section: 'Payment', sub_section: '', userData: userData, userName: userName, userRole: userRole });
});

var sendMonth="";
router.get("/paywithpaytm", (req, res) => {


    console.log("in  query" + JSON.stringify(req.query))

    var amount = req.query.amount
    console.log("in success paywithpaytm", + amount)

    var Month = req.query.month
     sendMonth= Month
    console.log("in success Month"+ JSON.stringify((Month)))
    console.log("in success amountMonth" + req.query.month)
   

    initPayment(req.query.amount).then(

        success => {
            res.render("paytm/paytmRedirect", {
                resultData: success,
                paytmFinalUrl: paytmConfig.PAYTM_FINAL_URL
            });
        },
        error => {
            res.send(error);
        }
    );
});


router.post("/paywithpaytmresponse", (req, res, next) => {

    responsePayment(req.body).then((success) => {
      
        if (success) {
            res.render("paytm/paytmResponse", { resultData: "true", responseData: success });
            let tenantId = req.session.tenantId;
            let tenantName = tenantConfig[tenantId].tenantName;
            let clientId = tenantConfig[tenantId].config.clientId;

            //console.log("success status >> >> " + success.STATUS)
            var cilentName = `${tenantName}_${sendMonth}`
             
            var params = {
                bankName: success.BANKNAME,
                ammount: success.TXNAMOUNT,
                date: success.TXNDATE,
                tenantName: cilentName,
                clientId: clientId,
               AmountMonth:sendMonth
            

            }
            
            listS3.putItemIntoDyanamodb(params, function (err, data) {
                if (err) console.log(err, err.stack); // an error occurred
                else console.log(data);           // successful response
            });
            if (success.STATUS === 'TXN_SUCCESS') {
                console.log('payment sucess')
            } else {
                console.log('payment failed')
            }
        } else {
            res.send(error);
        }
    })

});





module.exports = router;  
