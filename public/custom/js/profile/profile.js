
$(document).on("click", "#addRowBtn", function (e) {

    var div = $('<tr class="item" />');

    let _height = `<select class="form-control form-control-sm height" name="DynamicTextBox"> <option val="140">140</option> <option val="180">180</option> <option val="234">234</option> <option val="240">240</option> <option val="360">360</option> <option val="404">404</option> <option val="480">480</option> <option val="720">720</option> <option val="1080">1080</option> </select>`;
    let _width = `<select class="form-control form-control-sm width" name="DynamicTextBox"> <option val="250">250</option> <option val="320">320</option> <option val="352">352</option> <option val="416">416</option> <option val="640">640</option> <option val="720">720</option> <option val="1280">1280</option> <option val="1920">1920</option> </select>`;
    let _bitrate = `<select class="form-control form-control-sm bitrate" name="DynamicTextBox"> <option val="432000">432000</option> <option val="1160000">1160000</option> <option val="1440000">1440000</option> <option val="3288000">3288000</option> <option val="6400000">6400000</option> <option val="6864000">6864000</option> <option val="10400000">10400000</option> <option val="14392000">14392000</option> <option val="16000000">16000000</option> <option val="21448000">21448000</option> <option val="30688000">30688000</option> </select>`;
    let _frame = `<select class="form-control form-control-sm frame" name="DynamicTextBox"> <option val="25">25</option> </select>`;
    let _gop = `<select class="form-control form-control-sm gop"name="DynamicTextBox"> <option val="90">90</option> </select>`;
    let _audioBitrate = `<select class="form-control form-control-sm audio-bitrate" name="DynamicTextBox"> <option val="128000">128000</option> </select>`;
    let _maxBitrate = `<select class="form-control form-control-sm max-bitrate" name="DynamicTextBox"> <option val="1160000">1160000</option> <option val="3288000">3288000</option> <option val="6864000">6864000</option> <option val="14392000">14392000</option> <option val="21448000">21448000</option> <option val="30688000">30688000</option> </select>`;
    let _qvbrLevel = `<select class="form-control form-control-sm qvbr-level" name="DynamicTextBox"> <option val="7">7</option> </select>`;

    if ($('#rate_control').val() == 'cbr') {

        div.html(`<td class="text-center"><i class="mdi mdi-close-circle removeRowBtn" title="Remove Row"></i></td><td>${_height}</td><td>${_width}</td><td>${_bitrate}</td><td>${_frame}</td><td>${_gop}</td><td>${_audioBitrate}</td>`);
        $("#cbr-createProfilesTableBody").append(div);

    } else if ($('#rate_control').val() == 'qvbr') {

        div.html(`<td class="text-center"><i class="mdi mdi-close-circle removeRowBtn"></i></td><td>${_height}</td><td>${_width}</td><td>${_bitrate}</td><td>${_frame}</td><td>${_gop}</td><td>${_audioBitrate}</td><td>${_maxBitrate}</td><td>${_qvbrLevel}</td>`)
        $("#qvbr-createProfilesTableBody").append(div);

    } else {

        div.html(`<td class="text-center"><i class="mdi mdi-close-circle removeRowBtn"></i></td><td>${_height}</td><td>${_width}</td><td>${_bitrate}</td><td>${_frame}</td><td>${_gop}</td><td>${_audioBitrate}</td><td>${_maxBitrate}</td>`)
        $("#vbr-createProfilesTableBody").append(div);

    }

})


$(document).on("click", ".removeRowBtn", function (e) {
    $(this).closest("tr").remove();
})


$('#rate_control').on('change', function () {

    // Delete table rows except first row
    $("#cbr-createProfilesTableBody").find("tr:gt(0)").remove();
    $("#vbr-createProfilesTableBody").find("tr:gt(0)").remove();
    $("#qvbr-createProfilesTableBody").find("tr:gt(0)").remove();

    if (this.value == 'cbr') {
        $('#cbr-tbl').removeClass('force-hidden');
        $('#vbr-tbl').addClass('force-hidden');
        $('#qvbr-tbl').addClass('force-hidden');
    }
    if (this.value == 'vbr') {
        $('#cbr-tbl').addClass('force-hidden');
        $('#vbr-tbl').removeClass('force-hidden');
        $('#qvbr-tbl').addClass('force-hidden');
    }
    if (this.value == 'qvbr') {
        $('#cbr-tbl').addClass('force-hidden');
        $('#vbr-tbl').addClass('force-hidden');
        $('#qvbr-tbl').removeClass('force-hidden');
    }

});


$(document).ready(function () {

    // Create Profile
    $("#create-profile-form").submit(function (event) {

        var profiles = [];

        if ($('#rate_control').val() == 'cbr') {

            $("#cbr-createProfilesTableBody tr.item").each(function () {
                profiles.push({
                    height: $(this).find("select.height").val() ? $(this).find("select.height").val() : null,
                    width: $(this).find("select.width").val() ? $(this).find("select.width").val() : null,
                    bitrate: $(this).find("select.bitrate").val() ? $(this).find("select.bitrate").val() : null,
                    frame: $(this).find("select.frame").val() ? $(this).find("select.frame").val() : null,
                    gop: $(this).find("select.gop").val() ? $(this).find("select.gop").val() : null,
                    audio_bitrate: $(this).find("select.audio-bitrate").val() ? $(this).find("select.audio-bitrate").val() : null,
                })

            })
        } else if ($('#rate_control').val() == 'qvbr') {

            $("#qvbr-createProfilesTableBody tr.item").each(function () {
                profiles.push({
                    height: $(this).find("select.height").val() ? $(this).find("select.height").val() : null,
                    width: $(this).find("select.width").val() ? $(this).find("select.width").val() : null,
                    bitrate: $(this).find("select.bitrate").val() ? $(this).find("select.bitrate").val() : null,
                    frame: $(this).find("select.frame").val() ? $(this).find("select.frame").val() : null,
                    gop: $(this).find("select.gop").val() ? $(this).find("select.gop").val() : null,
                    audio_bitrate: $(this).find("select.audio-bitrate").val() ? $(this).find("select.audio-bitrate").val() : null,
                    qvbr_level: $(this).find("select.qvbr-level").val() ? $(this).find("select.qvbr-level").val() : null,
                    max_bitrate: $(this).find("select.max-bitrate").val() ? $(this).find("select.max-bitrate").val() : null,
                })

            })

        } else {

            $("#vbr-createProfilesTableBody tr.item").each(function () {
                profiles.push({
                    height: $(this).find("select.height").val() ? $(this).find("select.height").val() : null,
                    width: $(this).find("select.width").val() ? $(this).find("select.width").val() : null,
                    bitrate: $(this).find("select.bitrate").val() ? $(this).find("select.bitrate").val() : null,
                    frame: $(this).find("select.frame").val() ? $(this).find("select.frame").val() : null,
                    gop: $(this).find("select.gop").val() ? $(this).find("select.gop").val() : null,
                    audio_bitrate: $(this).find("select.audio-bitrate").val() ? $(this).find("select.audio-bitrate").val() : null,
                    max_bitrate: $(this).find("select.max-bitrate").val() ? $(this).find("select.max-bitrate").val() : null,
                })

            })

        }

        var data_string = {
            profile_name: $("#profile_name").val(),
            profiles: profiles
        };

        console.log("custom profile params > " + JSON.stringify(data_string))

        $.post('/v1/profile',
            data_string,
            function (data, status) {
                if (data.status === 200) {
                    toastr.success(data.message);
                    setTimeout(function () {
                        window.location.href = '/v1/Profile'
                    }, 3000)
                } else {
                    toastr.error('FAILED To Create Profile.');
                }
            })
        event.preventDefault()
    })

});
