const Notification = require('../app/models/notification');

class NotificationCls {
    NotificationCls() { }

    save(params) {

        const notificationDocument = new Notification({
            name: params.name ? params.name : null,
            description: params.description ? params.description : null,
            action: params.action ? params.action : null,
        });
        return new Promise((resolve, reject) => {
            notificationDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo Notification Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }

    findAll() {
        return new Promise((resolve, reject) => {
            Notification.find({})
                .sort({ created_at: -1 })
                .exec((err, Notification) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all notification has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.log(`Notification LENGTH> ${Notification.length}`);
                        resolve(Notification);
                    }
                });
        });
    }

    removeNotification() {
        return new Promise((resolve, reject) => {
            Notification.deleteMany({}, function (err, result) {
                if (err) {
                    console.log(err);
                }
                console.log(result);
            });
        })

    }



}

module.exports = {
    NotificationClass: NotificationCls,
};