
// get video duration >> 

// create the video element but don't add it to the page
var vid = document.createElement('video');
document.querySelector('#files1').addEventListener('change', function () {
    // create url to use as the src of the video
    var fileURL = URL.createObjectURL(this.files[0]);
    vid.src = fileURL;
    // wait for duration to change from NaN to the actual duration
    vid.ondurationchange = function () {
        //alert(this.duration);
    };
});


// For getting file name in text box
$('input[name=files1]').change(function (ev) {
    var file = $('#files1')[0].files[0];
    var filename = file.name; // filename with extension
    filename = filename.substr(0, filename.lastIndexOf('.'));
    $('#file_name').val(filename);
});


$(document).ready(function () {


    
function getCloudFront() {
    return new Promise((resolve, reject) => {
        $.get('/v1/video/getCloudFront',
            
            function (data, status) {
                if (data.status === 200) {
                    resolve(data)
                 
                } else {
                    toastr.error(data.message);
                }
            })
        })
    }
    
    // video upload >>
    $("#upload-asset-form").submit(function (event) {
           
        var formData1 = new FormData();

        formData1.append('videoUpload', $('#files1')[0].files[0]);
        formData1.append('file_name', $('#file_name').val() ? $('#file_name').val() : null);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                $(".modal_progress_btn").click()
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $(".progress-div").css("display", "block")
                        $("#percent_complete").text(percentComplete + " %");
                        $(".progress-bar-video").css("width", percentComplete + '%');

                        if (percentComplete === 100) {
                            console.log("*****COMPLETE DONEEEEE******");
                        }

                    }
                }, false);

                return xhr;
            },
            // url: '/v1/video'  ,
            url: '/v1/video/',
            type: 'POST',
            data: formData1,
            cache: false,
            contentType: false,
            processData: false
        }).then(function (data , err) {
            // console.log("DATA: " + JSON.stringify(data))
            if (data.status == 200 || data.status == 201) {

                toastr.success('Asset Uploaded Successfully.');
                setTimeout(function () {
                    window.location.href = '/v1/video'
                }, 3000);

            } else if (err){
                console.log("Oops! Video Save ERROR >  " +err)

            }
        })
   
        event.preventDefault()
    })
})