




function destroyRows(dataTblParams) {
    $(`#${dataTblParams.tblbody}`).empty()
    $(`#${dataTblParams.tblId}`).DataTable().rows().remove();
    $(`#${dataTblParams.tblId}`).DataTable().destroy()
}


// for formatting bytes to readable file size
function formatBytes(x) {

    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }
    //include a decimal point and a tenths-place digit if presenting 
    //less than ten of KB or greater units
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
}


// dynamic currency of USD ,INR call in costCalculation function
function currency() {
    return new Promise((resolve, reject) => {
        $.get('/v1/invoice/exchangeratesapi',

            function (data, status) {
                if (data) {

                    var currencyRates = {
                        _USD: data.rates.USD,
                        _INR: data.rates.INR
                    }
                    resolve(currencyRates)

                } else {
                    reject()
                }
            })
    })
}


function getInvoiceDate() {

    var selectedDate = $('#invoiceDate').val();

    localStorage.setItem('selectedDate', selectedDate)

    const month = (selectedDate).substr(0, (selectedDate).indexOf('-'));
    const year = selectedDate.slice(selectedDate.lastIndexOf('-') + 1);
    function getNumberOfDays(year, month) {
        var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
        return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    }
    var monthNumber, numberOfDays;
    switch (month) {
        case 'Jan':
            monthNumber = '01'
            numberOfDays = getNumberOfDays(year, 0) // Note : that month is zero-based (it must be between 0 and 11)
            break;
        case 'Feb':
            monthNumber = '02'
            numberOfDays = getNumberOfDays(year, 1)
            break;
        case 'Mar':
            monthNumber = '03'
            numberOfDays = getNumberOfDays(year, 2)
            break;
        case 'Apr':
            monthNumber = '04'
            numberOfDays = getNumberOfDays(year, 3)
            break;
        case 'May':
            monthNumber = '05'
            numberOfDays = getNumberOfDays(year, 4)
            break;
        case 'Jun':
            monthNumber = '06'
            numberOfDays = getNumberOfDays(year, 5)
            break;
        case 'Jul':
            monthNumber = '07'
            numberOfDays = getNumberOfDays(year, 6)
            break;
        case 'Aug':
            monthNumber = '08'
            numberOfDays = getNumberOfDays(year, 7)
            break;
        case 'Sep':
            monthNumber = '09'
            numberOfDays = getNumberOfDays(year, 8)
            break;
        case 'Oct':
            monthNumber = '10'
            numberOfDays = getNumberOfDays(year, 9)
            break;
        case 'Nov':
            monthNumber = '11'
            numberOfDays = getNumberOfDays(year, 10)
            break;
        case 'Dec':
            monthNumber = '12'
            numberOfDays = getNumberOfDays(year, 11)
            break;
        default:
        // code block
    }
    var startDate = `${year}-${monthNumber}-01`; // format('YYYY-MM-DD')
    var endDate = `${year}-${monthNumber}-${numberOfDays}`; // format('YYYY-MM-DD')
    var obj = { startDate: startDate, endDate: endDate }
    return obj
}


function getDatafromDyanamoDb(startDateparams) {
    return new Promise((resolve, reject) => {
        $.post(`/v1/video/getDatafromDyanamoDb`,
            startDateparams,
            function (data, status) {
                if (data.status == 200) {
                    console.log(JSON.stringify(data))
                    resolve(data)
                }
            }
        )
    })
}

// Tenant Invoice 
$(document).on("click", ".tenantInvoice", function () {


    var date = getInvoiceDate(this.obj)
    var startDate = date.startDate
    var endDate = date.endDate

    var previewTotalCost, basicTotalCost, standardTotalCost, premiumTotalCost, totalStorageCost

    // get all Assets from s3 bucket
    function getStorageCost() {

        return new Promise(async (resolve, reject) => {
            var currencyRates = await currency()
            $.post(`/v1/video/listS3_AssetsList`,
                function (data, status) {
                    if (data.status == 200) {

                        console.log('get storage cost >> >>' + JSON.stringify(data))

                        var dataTblParams = {

                            tblId: 'list_table', // data table ID
                            tblbody: 'list_tbody' // data table body ID
                        }
                        destroyRows(dataTblParams);
                        var options_table = "";
                        var bucketSize = 0;
                        var _data = data.data.Contents;
                        var rs_perGBperMonth = 1.65288;  // Rs. per GB per month
                        var rs_perGBperDay = rs_perGBperMonth / 30; // Rs. per GB per day
                        var index = 1;
                        for (var i = 0; i < _data.length; i++) {

                            fileSize = _data[i].Size;

                            LastModified = _data[i].LastModified;
                            bucketSize = fileSize + bucketSize;
                            totalBucketSize = formatBytes(bucketSize);
                            LastModifiedDate = new Date(LastModified)   //LastModified
                            var date = new Date()// date
                            var Difference_In_Time = date - LastModifiedDate;
                            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);

                            formatedFileSize = formatBytes(fileSize);
                            console.log('\n formatedFileSize ' + formatedFileSize)
                            fileSizeUnit = formatedFileSize.slice(-2);
                            console.log('\nfileSize ' + fileSizeUnit)
                            fileSizeUnitValue = formatedFileSize.slice(0, -2);
                            console.log('\n fileSizeUnitValue ' + fileSizeUnitValue)
                            switch (fileSizeUnit) {

                                case `GB`:
                                    var GB = rs_perGBperDay;
                                    totalStorageCost = Difference_In_Days * fileSizeUnitValue * GB;
                                    console.log('GB >> >>' + totalStorageCost)
                                    break;
                                case `MB`:
                                    var MB = 0.00005380;
                                    totalStorageCost = Difference_In_Days * fileSizeUnitValue * MB;
                                    console.log('MB >> >>' + totalStorageCost)

                                    break;
                                case `KB`:
                                    var KB = 0.000000055096;
                                    totalStorageCost = Difference_In_Days * fileSizeUnitValue * KB;
                                    console.log('\n KB >> >>' + totalStorageCost)
                                    break;
                                case `es`:
                                    totalStorageCost = 0;
                                    console.log('\n KB >> >>' + totalStorageCost)
                                    break;

                                default:
                            }


                            if (i === _data.length - 1) {
                                options_table = '<tr class="invoice-tbl-row invoice-row invoice-table">' +
                                    '<td width="5%">' + index + '</td>' +
                                    '<td class="asset-name">s3 Storage</td>' +
                                    '<td width="20%">' + totalBucketSize + '</td>' +
                                    '<td width="20%" span class="WebRupee">Rs.</span>' + (totalStorageCost.toFixed(2)) + '</td>'
                                index++
                                $('#list_tbody').append(options_table)
                                resolve()
                            }
                        }
                    }
                });
        })

    }


    //get transcode cost
    function getTranscodeCost() {

        return new Promise(async (resolve, reject) => {

            var currencyRates = await currency()

            $.post(`/v1/invoice/getProfiles`,
                function (data, status) {

                    //    console.log('get Transcode cost >> >>' +JSON.stringify(data))


                    if (data.status === 200) {

                        var dataTblParams = {
                            tblId: 'list_transcode_table', // data table ID
                            tblbody: 'list_transcode_tbody' // data table body ID
                        }
                        destroyRows(dataTblParams);

                        _data = data.data
                        var previewAssets = _data[0].Items
                        var basicProfileAssets = _data[1].Items
                        var standardProfileAssets = _data[2].Items
                        var premiumProfileAssets = _data[3].Items
                        var totalvideolengthminutes = 0;
                        var resolution = 0.0075 / currencyRates._USD;
                        //alert('resolution >> >> ' + JSON.stringify(resolution))
                        var ResolutionInRs = resolution * currencyRates._INR;
                        // alert('ResolutionInRs >> >> ' + JSON.stringify(ResolutionInRs))

                        var framerate = 25;
                        var index = 1;

                        for (var i = 0; i < _data[0].Count; i++) {

                            previewAssetsData = previewAssets[i].videolengthminutes ? previewAssets[i].videolengthminutes : 0
                            totalvideolengthminutes += parseInt(previewAssetsData)


                        }
                        //alert('totalvideolengthminutes >> >> ' + JSON.stringify(totalvideolengthminutes))
                        previewTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        //alert('previewTotalCost >> >> ' +JSON.stringify(previewTotalCost))
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td>preview</td>' +
                            '<td width="20%">' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (previewTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        for (var j = 0; j < _data[1].Count; j++) {

                            basicAssetsData = basicProfileAssets[j].videolengthminutes ? basicProfileAssets[j].videolengthminutes : 0
                            totalvideolengthminutes += parseInt(basicAssetsData)

                        }

                        basicTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td> Basic </td>' +
                            '<td width="20%">' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (basicTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        for (var k = 0; k < _data[2].Count; k++) {

                            standardAssetsData = standardProfileAssets[k].videolengthminutes ? standardProfileAssets[k].videolengthminutes : 0
                            totalvideolengthminutes += parseInt(standardAssetsData)
                        }
                        standardTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td> Standard </td>' +
                            '<td>' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (standardTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        for (var l = 0; l < _data[3].Count; l++) {

                            premiumAssetsData = premiumProfileAssets[l].videolengthminutes ? premiumProfileAssets[l].videolengthminutes : 0
                            videoLength = parseInt(premiumAssetsData)
                            totalvideolengthminutes += videoLength
                        }
                        premiumTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td> premium </td>' +
                            '<td>' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (premiumTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        setTimeout(function () {
                            resolve()
                        }, 500);

                    }
                    else {
                        toastr.error(data.message);
                    }
                });
        })
    }


    const promise1 = getStorageCost();
    const promise2 = getTranscodeCost();

    Promise.all([promise1, promise2]).then(function (response) {

        var totalTranscodprofiles = previewTotalCost + basicTotalCost + standardTotalCost + premiumTotalCost;
        var total = totalTranscodprofiles + totalStorageCost
        $("#total").html(`INR ` + (total.toFixed(2)))
        var totalData = total.toFixed(2)
        localStorage.setItem('totalCostValue', JSON.stringify(totalData))

        $('#invoice-div').show();
        //$('.tenant-name').html(`<strong>Name: </strong>` + $("#tenant option:selected").text())

    })

    var startDateparams = {
        startDate: startDate
    }

    getDatafromDyanamoDb(startDateparams).then((getDatafromDyanamoDb) => {

        var newDate = new Date(startDate).toDateString()
        var arr1 = newDate.split(' ');
        var month = arr1[1]
        var year = arr1[3]
        CompareDate = month + '-' + year;

        var Paidstatus = getDatafromDyanamoDb.data.Item.status.S;
        var amountMonth = getDatafromDyanamoDb.data.Item.AmountMonth.S;


        if (amountMonth === CompareDate && Paidstatus === "paid") {

            $('#payId').hide(0);

            $('#paidAmount').show(0);
            $('#totalPaid').show(0);

            $('#totalPaid').removeClass("col-md-6")
            $('#totalPaid').addClass("col-md-3")

            toastr.success('Payment already Paid')
        } else {
            $('#payId').show(0);
            $('#paidAmount').hide(0);
            $('#totalPaid').show(0);



            $('#totalPaid').removeClass("col-md-3")
            $('#totalPaid').addClass("col-md-6")
        }


    })

})

// get of Tenant listing call 
$(function () {

    $('#invoice-div').hide()
})

// code for show hide button of userCreate & groupCreate 
$(document).ready(function () {

    $('#createbtn').hide(0);  //or do it through css
    $('#usertab').click(function () {
        $('#deletebtn').hide(0);
        if (!createAdminUserFlag) {
            $('#createbtn').show(0);
        }
    });

})

$(document).ready(function () {
    totalCost = JSON.parse(localStorage.getItem('totalCostValue'));
    selectedDate = (localStorage.getItem('selectedDate'));
    $("#amount").val(totalCost)
    $("#month").val(selectedDate)

})


$("#paynowForm").submit(function (event) {

    event.preventDefault();
    window.location.href = `/v1/paytm/paywithpaytm?month=${$("#month").val()}&amount=${$("#amount").val()}`;

});

