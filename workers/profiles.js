const Profiles = require('../app/models/profiles');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// ##################################################################################
// model here

const ProfileSchema = new Schema({ content_id: { type: String }, profile_name: { type: String, required: true }, profiles: { type: Object, require: true }, mediaJSON: { type: Object, require: true }, created_at: Date, updated_at: Date, created_by: { type: String }, }, { toJSON: { virtuals: true } });
ProfileSchema.pre('save', function (next) { if (this.isNew) { console.log(' IS NEW CALLED!! '); this.created_at = new Date(); this.updated_at = new Date(); } else { console.log(' IS NEW IS FALSE!! '); this.updated_at = new Date(); } next(); });
// ##################################################################################



class ProfilesCls {
    ProfilesCls() { }

    save(params) {

        return new Promise(async (resolve, reject) => {

            // accesing our cluster connection from global object
            const clientConnection = await global.clientConnection;
            const db = await clientConnection.useDb(params.mongoDbName);
            const Profiles = await db.model('Profiles', ProfileSchema, 'Profiles');

            const profileDocument = new Profiles({
                profile_name: params.profile_name ? params.profile_name : null,
                profiles: params.profiles ? params.profiles : null,
                mediaJSON: params.mediaJSON ? params.mediaJSON : null,
            });

            profileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo Profile Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }

    findall(params) {
        return new Promise(async (resolve, reject) => {

            // accesing our cluster connection from global object
            const clientConnection = await global.clientConnection;
            const db = await clientConnection.useDb(params.mongoDbName);
            const Profiles = await db.model('Profiles', ProfileSchema, 'Profiles');

            Profiles.find({})
                .sort({ _id: -1 })
                .exec((err, Profile) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all Profile has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.log(`Profile LENGTH> ${Profile.length}`);
                        resolve(Profile);
                    }
                });
        });
    }

    deleteProfile(params) {

        return new Promise(async (resolve, reject) => {

            // accesing our cluster connection from global object
            const clientConnection = await global.clientConnection;
            const db = await clientConnection.useDb(params.mongoDbName);
            const Profiles = await db.model('Profiles', ProfileSchema, 'Profiles');

            const obj = {};
            Profiles.deleteOne({ _id: params._id })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Profile SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }


    findDataByid(params) {
        return new Promise(async (resolve, reject) => {

            // accesing our cluster connection from global object
            const clientConnection = await global.clientConnection;
            const db = await clientConnection.useDb(params.mongoDbName);
            const Profiles = await db.model('Profiles', ProfileSchema, 'Profiles');

            Profiles.findOne({ profile_name: params.profile_name })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    // find Media JSON
    findMediaJson(params) {
        return new Promise(async (resolve, reject) => {

            // accesing our cluster connection from global object
            const clientConnection = await global.clientConnection;
            const db = await clientConnection.useDb(params.mongoDbName);
            const Profiles = await db.model('Profiles', ProfileSchema, 'Profiles');

            Profiles.findOne({ profile_name: params.profile })
                .exec((err, Profile) => {
                    if (err) {
                        console.error(`Error :: Mongo Find Profile has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.log(`Profile in mongodb database > ${Profile}`);
                        resolve(Profile);
                    }
                });
        });
    }

}

module.exports = {
    ProfilesClass: ProfilesCls,
};