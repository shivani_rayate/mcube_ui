const express = require('express');
const router = express.Router();
const download = require('download');
const fs = require('fs');
const util = require('util');
const AWS = require('aws-sdk');

const awsConfig = require('../config/aws_config')['development'];
AWS.config.update({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
  region: awsConfig.region,
});



const cognito = require('../app/utilities/tenant_management/cognito');


const Notification = require("../workers/notification");
const notificationObj = new Notification.NotificationClass();

const tenantConfig = require('../config/tenantConfig');

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-3 ##########
let mcube_cognito_BASE = API.mcube_cognito.base.development;
let mcube_transcode_BASE = API.mcube_transcode.base.development;
let mcube_adv_transcode_BASE = API.mcube_adv_transcode.base.development;



router.get('/', function (req, res, next) {

  let user_id = req.session ? (req.session.user_id ? req.session.user_id : null) : null;

  console.log("req.session>.user_id >> " + JSON.stringify(user_id))
  if (user_id) {

    console.log("I SHOULD REDIREC!!")

    res.redirect('/v1/video')

  } else {
    console.log("I SHOULD LOGIN!")
    res.render('login');
  }
});

router.get('/lockscreen', function (req, res, next) {
  res.render('other/lockscreen');
});

router.get('/page_404', function (req, res, next) {
  res.render('other/page_404');
});

router.get('/page_500', function (req, res, next) {
  res.render('other/page_500');
});

router.get('/temp', function (req, res, next) {
  res.render('temp');
});

router.get('/tempp', function (req, res, next) {
  res.render('tempp');
});

router.get('/temppp', function (req, res, next) {
  res.render('temppp');
});

router.get('/authenticateUser', function (req, res, next) {
  res.render('user/authenticateUser');
});

router.get('/forgotPassword', function (req, res, next) {
  res.render('user/forgotPassword');
});

// Login
router.post('/login', (req, res, next) => {
  console.log("\n /login called in UI")

  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.login,
    data: {
      tenantId: req.body.tenantId,
      username: req.body.username,
      password: req.body.password,
    }
  }
  
  requestMethods.postApiRequest(params).then((response) => {

    if (response.status === 200) {
      console.log('user sign in response >> ' +JSON.stringify(response.data.responseData));
      req.session.user_id = "ADMIN";

      //set user details in sesssion
      req.session.userData = response.data.responseData;
      req.session.userName = response.data.responseData.idToken.payload.email;
      req.session.userRole = (response.data.responseData.idToken.payload['cognito:groups']) ? (response.data.responseData.idToken.payload['cognito:groups']) : 'User';
      req.session.cloudFront = response.data.cloudFront;
      var payload_iss = response.data.responseData.idToken.payload.iss;
      req.session.tenantId = (payload_iss).slice(payload_iss.lastIndexOf('/') + 1);

      console.log("\n USER Info >>>>>   ")
      console.log(`\n userData>>>>>>>>>>>>>>>>>>>> + ${util.inspect(req.session.userData)}`)
      console.log("\n userName>>>>>>>>>>>>>>>>>>>>   " + req.session.userName)
      console.log("\n userRole>>>>>>>>>>>>>>>>>>>>  " + req.session.userRole)

      res.json(response);

    } else {
      res.json(response);
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


// authenticateUser with AWS Cognito
router.post('/authenticateUser', (req, res, next) => {
  console.log("authenticateUser with AWS Cognito called");

  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.authenticateUser,
    data: {
      tenantId: req.body.tenantId,
      username: req.body.username,
      password: req.body.password,
      newPassword: req.body.newPassword,
      confirmPassword: req.body.confirmPassword
    }
  }
  // console.log('authenticate user' +JSON.stringify(params));

  requestMethods.postApiRequest(params).then((response) => {
    console.log('authenticate user' + JSON.stringify(response));
    res.json(response);
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// Forgot Password
router.post('/forgotPassword', (req, res, next) => {
  console.log("forgotPassword API called");

  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.forgotPassword,
    data: {
      username: req.body.username,
      tenantId: req.body.tenantId
    }
  }

  requestMethods.postApiRequest(params).then((response) => {
    console.log('forgotPassword user' + JSON.stringify(response));
    res.json(response);
  }).catch((err) => {
    console.log(`error IN ForgotPassword : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// Reset Password
router.post('/resetPassword', (req, res, next) => {
  console.log("resetPassword API called");

  let params = {
    url: mcube_cognito_BASE + API.mcube_cognito.users.resetPassword,
    data: {
      confirmationCode: req.body.verificationCode,
      password: req.body.password,
      username: req.body.username,
      tenantId: req.body.tenantId
    }
  }

  requestMethods.postApiRequest(params).then((response) => {
    console.log('resetPassword user' + JSON.stringify(response));
    res.json(response);
  }).catch((err) => {
    console.log(`error IN resetPassword : ${err}`);
    res.json({
      status: 500,
      message: err.message,
      data: null,
    });
  });
})


//Logout
router.get('/logout', function (req, res) {

  req.session.destroy(function (err) {
    if (err) {
      res.redirect('/');
    } else {
      req.session = null;
      console.log("Logout Success " + JSON.stringify(req.session) + " ");
      res.redirect('/login');
    }
  });
});


router.get('/404', function (req, res, next) {
  console.log('BEFORE')

  setTimeout(() => {
    console.log('END OF SET TIMEOUT')
    res.render('404', { title: '404 Error' });
  }, 120000);
});


router.get('/downloadFile', (req, res) => {

  console.log('Download called >>');

  var url = req.query.url;
  var options = {
    filename: 'package.mp4'
  }

  download(url, 'download', options).then((data) => { // (url, directory, options)

    res.download('download/package.mp4', function (err) {
      if (err) throw err
      console.log("Downloaded!")
      fs.unlink('download/package.mp4', function (err) {
        if (err) throw err;
        console.log('File deleted!');

      });
    })
  })

})

// save notification 
router.post('/notification', (req, res, next) => {
  console.log("Notification POST called")

  const params = {
    name: req.body.name ? req.body.name : null,
    description: req.body.description ? req.body.description : null,
    // action: req.body.action? req.body.action : null,

  }
  console.log("notification data > " + JSON.stringify(params))

  notificationObj.save(params).then((_response) => {
    if (_response) {
      console.log('Notification data saved');
      res.json({
        status: 200,
        message: 'Notification Saved successfuly',
        data: _response,
      })

    } else {
      console.log('Notification data save failed');
      res.json({
        status: 401,
        message: 'Notification Save failed',
        data: null,
      })
    }
  })


})

router.get('/notification', (req, res, next) => {
  console.log("get notification data");

  notificationObj.findAll().then((Notification) => {
    res.json({
      status: 200,
      message: 'notification successfully',
      data: Notification,
    })

  })
})

router.post('/removeNotification', (req, res, next) => {
  console.log("remove notifications Post called ")
  notificationObj.removeNotification().then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: "Remove Notifications successfully",
        data: response
      })

    } else {
      res.json({
        status: 400,
        message: "failed",
        data: null
      })
    }
  });
})



// Signup
router.get('/signup', function (req, res, next) {
  res.render('signup');
});


// registration
router.post('/signup', (req, res, next) => {

  console.log("signup POST called in UI")

  var tenantId = req.body.tenantId;

  // get tenant config from dynamodb first
  var docClient = new AWS.DynamoDB.DocumentClient();

  var _params = {
    TableName: awsConfig.cloudformation.configDynamodbTable,
    Key: {
      'poolName': tenantId
    }
  };

  const getTenantPromise = docClient.get(_params).promise();

  getTenantPromise.then(
    (data) => {

      if (data.Item && data.Item.poolName === tenantId) {

        const params = {
          UserPoolId: data.Item.poolId,
          name: req.body.name ? req.body.name : null,
          email: req.body.email ? req.body.email : null,
          password: req.body.password ? req.body.password : null
        };

        cognito.Signup(params).then((response) => {
          if (response) {
            //console.log('user data saved response >> ' + JSON.stringify(response));
            res.json({
              status: 200,
              message: 'User signup Successfuly',
              data: response,
            })
          }
        }).catch((err) => {
          console.log(`\n error IN USER signup: ${err.message}`);
          res.json({
            status: 500,
            message: err.message,
            data: null,
          });
        });

      } else {
        res.json({
          status: 403,
          message: "Incorrect Tenant ID or Alias",
          data: null
        })
      }
    },
    (err) => {
      console.error("\n Unable to scan the tenantInfo table. Error JSON:", JSON.stringify(err, null, 2));

      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    });

});


module.exports = router;