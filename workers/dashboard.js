/* eslint-disable no-param-reassign */
const moment = require('moment');
const _ = require('lodash');
const util = require('util');

class DashboardCls {

    DashboardCls() { }

    save(params) {
        return new Promise((resolve, reject) => {
            const videoDocument = new Video({
                content_id: params.content_id ? params.content_id : null,
                file_name: params.file_name ? params.file_name : null,
                video_id: params.fileName ? params.fileName : null,
                duration: params.duration ? params.duration : null,
                title: params.title ? params.title : null,
                summary_short: params.summary_short ? params.summary_short : null,
                status: params.status ? params.status : null,
                keywords: params.keywords ? params.keywords : null,
                language: params.language ? params.language : null,
                description: params.description ? params.description : null,
                cast: params.cast ? params.cast : null,
                is_featured: params.is_featured ? params.is_featured : null,
                is_popular: params.is_popular ? params.is_popular : null,

                created_by: 'Admin',
            });
            console.log('SAVE');
            videoDocument.save((err, videoData) => {
                if (err) {
                    console.log(`err in saving video: ${err}`);
                    reject(err);
                } else {
                    console.log(`SAVED VIDEO DATA ${util.inspect(videoData)}`);
                    resolve(videoData);
                }
            });
        });
    }

   
    findall(){
        return new Promise((resolve, reject) => {
            Video.find({})
                .sort({ created_at: -1 })
                .exec((err, videos) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all videos has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        //console.log(`VIDEOS LENGTH> ${videos.length}`);
                        resolve(videos);
                    }
                });
        });
    }

    findDataByid(params){
        return new Promise((resolve, reject) => {
            Video.findOne({ content_id: params.content_id }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }
}

module.exports = {
    DashboardClass: DashboardCls,
};