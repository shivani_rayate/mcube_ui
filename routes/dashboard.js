var express = require('express');
var router = express.Router();

const AWS = require('aws-sdk');

const Video = require('../workers/dashboard');
const dashboardObj = new Video.DashboardClass();

let video_utility = require('../app/utilities/video');

const awsConfig = require('../config/aws_config')['development'];

var moment = require('moment');


AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const s3 = new AWS.S3();

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-1 ##########
let mcube_transcode_BASE = API.mcube_transcode.base.development;


//####################

router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/index', { section: 'Dashboard', sub_section: '', userData: userData, userName: userName, userRole: userRole });
});

router.get('/proccessing_list', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/proccessing_list', { section: 'Dashboard', sub_section: 'Proccessing', userData: userData, userName: userName, userRole: userRole });
});

router.get('/todays_uploaded_list', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/todays_uploaded_list', { section: 'Dashboard', sub_section: 'Todays Uploaded', userData: userData, userName: userName, userRole: userRole });
});


router.get('/total_transcoded_list', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/total_transcoded_list', { section: 'Dashboard', sub_section: 'Total Transcoded', userData: userData, userName: userName, userRole: userRole });
});

// get todaysuploaded_list 
router.post('/todays_uploaded', (req, res, next) => {

    const params = {
    };

    dashboardObj.countTodaysVideo(params).then((data) => {
        res.json({
            status: 200,
            message: 'Todays Videos Counted successfully',
            data: data,
        });
    });

});


// get processing_list 
router.post('/proccessing_list', (req, res, next) => {

    const params = {
        url: mcube_transcode_BASE + API.mcube_transcode.dashboard.proccessingList,
        data: {
            profiles: { basic: { status: "PROCESSING" } }
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }


    };
   //console.log('***GET PROCESSING LIST PARAMS ***' + JSON.stringify(params));
      requestMethods.postApiRequestWithHeader(params).then((data) =>{
      res.json(data);
   });
});



// get transcoded_list 
router.post('/transcoded_list', (req, res, next) => {

    const params = {
                   url: mcube_transcode_BASE + API.mcube_transcode.dashboard.transcoded_list,
                   data:{

                       },
                       headers: {
                        token: req.session.userData.idToken.jwtToken
                    }

    };
     console.log('***GET TRANSCODED LIST PARAMS ***' + JSON.stringify(params));
     requestMethods.postApiRequestWithHeader(params).then((data) =>{
        res.json(data);
     });
  
});

module.exports = router;