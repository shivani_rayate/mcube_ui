// document.ready
$(function () {

    // get all Assets from s3 bucket
    $.get('/v1/video/listS3_Assets',
        function (data, status) {
            console.log("s3 bucket overview >> >> "+JSON.stringify(data))
            if (data.status == 200) {
                console.log("S3 Listing VIDEOS: " + JSON.stringify(data))
                destroyRows();
                filterS3List(data)
            }
        });

})


function destroyRows() {
    $('#list_s3_assets_tbody').empty()
    $('#list_s3_assets_table').DataTable().rows().remove();
    $("#list_s3_assets_table").DataTable().destroy()
}

const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

function niceBytes(x) {
    let l = 0, n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }
    //include a decimal point and a tenths-place digit if presenting 
    //less than ten of KB or greater units
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
}


// function to filter the S3 list
function filterS3List(data) {

    var contents_arr = data.data.Contents;
    var assets_arr = [];

    for (var i = 0; i < contents_arr.length; i++) {
        var extension = contents_arr[i].Key.split('.').pop();
        if (extension === 'mov' || extension === 'mp4' || extension === 'mxf')
            assets_arr.push(contents_arr[i])

        if (i === (contents_arr.length - 1))
            appendVideosDatatable(assets_arr);
    }
}

// UUID Generate
function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}


// File Package Selection
$(document).on("click", ".file-overview-table", function (e) {

    $('.file-overview-table').removeClass("active");
    $(this).addClass("active");

});

function appendVideosDatatable(assets_arr) {
    var array = assets_arr;
    var options_table = "";

    array.forEach(function (element, i) {

        var _id = uuidv4();


        var asset_name = element.Key ? element.Key : "";
        asset_name = asset_name.slice(asset_name.lastIndexOf('/') + 1); // need to remove prefixes of file name as s3list api doesnt support list from folder contained in s3 bucket

        var modified_on = element.LastModified ? moment(element.LastModified).format('lll') : "";
        var asset_size = element.Size ? element.Size : "";

        options_table += '<tr class="s3-list-asset-tbl-row asset-row file-overview-table" id=' + _id + '>' +
            '<td class="">' + (i + 1) + '</td>' +
            '<td class="asset-name">' + asset_name + '</td>' +
            '<td>' + modified_on + '</td>' +
            '<td>' + niceBytes(asset_size) + '</td>' +
            '<td class="action-td" id=' + _id + '><div class="dropdown"><a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"> <a href="#" class="dropdown-item create-preview">Create Preview</a> <a href="#" class="dropdown-item">Delete</a> </div> </div></td>'

        if (i == array.length - 1) {
            $('#list_s3_assets_tbody').append(options_table)
            reInitializeDataTable()
        }

    })
}


function reInitializeDataTable() {
    $("#list_s3_assets_table").DataTable().destroy()
    list_s3_assets_table = $('#list_s3_assets_table').DataTable({
        //"order": [[2, "desc"]], // for descending order
        "columnDefs": [
            { "width": "40%", "targets": 1 }
        ]
    })
}



// create-preview
$(document).on("click", ".create-preview", function () {

    var params = {
    }

    $.post('/v1/video/create-preview',
        params,
        function (data, status) {
            if (data.status === 200) {
                toastr.success(data.message);
            } else {
                toastr.error(data.message);
            }
        })

})