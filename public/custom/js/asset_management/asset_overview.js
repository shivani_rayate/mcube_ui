

var player = videojs('asset-player');
var assetPreviewPlayer = videojs('preview-asset-player');

// Initiate Asset Preview Player
function initiateAssetPreviewPlayer(player_id, url) {
    return new Promise((resolve, reject) => {
        assetPreviewPlayer.src({
            type: 'application/x-mpegURL',
            src: url
        });
        assetPreviewPlayer.selectorQuality();
        resolve();
    })
}


// Initiate Asset Overview Player
function initiatePlayer(player_id, url) {
    return new Promise((resolve, reject) => {
        player.src({
            type: 'application/x-mpegURL',
            src: url
        });
        player.selectorQuality();
        resolve();
    })
}

// get all Assets from mongo db
function list_all_assets() {

    // Initialization
    $("#list_all_video_table").DataTable();

    // get all videos
    $.get('/v1/video/fetch_all_videos',
        function (data, status) {
            if (data.status == 200) {
                destroyRows();
                appendVideosDatatable(data);
            } else {
                // $('#list_all_video_tbody').append('No data available') // appending null for data not found
            }
        });

    // get all custom transcode profiles
    $.get('/v1/profile/getAllProfiles',
        function (data, status) {

            if (data.status == 200) {
                //console.log("getAllProfiles: " + JSON.stringify(data))
                let profiles_data = data.data;
                var profiles_options = '';
                profiles_data.forEach(function (element, i) {
                    profiles_options += `<option data-transcode_profile_type="custom" value="${element.profile_name}">${element.profile_name}</option>`
                })
                $('#transcode-profiles').append(profiles_options);
            }
        });
}




// document.ready
$(function () {
    list_all_assets();
});



// Refresh data table
$(document).on("click", "#list-all-assetstable-refresh-btn", function () {

    list_all_assets();
})

function destroyRows() {
    $('#list_all_video_tbody').empty()
    $('#list_all_video_table').DataTable().rows().remove();
    $("#list_all_video_table").DataTable().destroy()
}


function appendVideosDatatable(data) {

    console.log(JSON.stringify(data))
    var array = data.data;
    var options_table = "";

    array.forEach(function (element, i) {

        var content_id = element.content_id ? element.content_id : "";
        var title = element.title ? element.title : "";
        var updated_at = element.updated_at ? moment(element.updated_at).format('lll') : "";
        var created_by = element.created_by ? element.created_by : "";
        var status = element.media_job_status ? element.media_job_status : "PROGRESSING";
        var preview_url = element.preview_url ? element.preview_url : "";
        var video_path = element.video_path ? element.video_path : "";
        var file_name = element.file_name ? element.file_name : "";
        var thumbnail_pic = element.thumbnail_pic ? element.thumbnail_pic : "";
        var previewCloudFront_Url = element.previewCloudFront_Url ? element.previewCloudFront_Url : "";
       
        if (status === 'COMPLETE') {
            status = '<span class="badge badge-success">Available</span>'
        } else if (status === 'PROGRESSING') {
            status = '<span class="badge badge-purple">Processing</span>'
        } else {
            status = '<span class="badge badge-danger">Error</span>'
        }

        options_table += `<tr class="asset-overview-tbl-row asset-row" id=${content_id} data-thumbnail_pic=${thumbnail_pic}>
            <td class="asset-title">${file_name}</td>
            <td>${updated_at}</td>
            <td class="st">${status}</td>
            <td class="action-td" id=${content_id}> <div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"> <a href="#" class="dropdown-item preview-video" data-toggle="modal" data-target="#previewVideo" data-preview_url=${preview_url} data-video_path=${video_path} cloudFront_video=${previewCloudFront_Url}>Preview</a> <a href="#" class="dropdown-item delete-video">Delete</a> <a href="#"  cloudFront_video=${previewCloudFront_Url} class="dropdown-item getlink-preview" >Get Link</a></td>`;

        if (i == array.length - 1) {
            $('#list_all_video_tbody').append(options_table)
            reInitializeDataTable().then(() => {
                //initiateAssetSelection
                initiateAssetSelection();
            })
        }
    })
}

var list_all_video_table;

function reInitializeDataTable() {
    return new Promise((resolve, reject) => {

        $("#list_all_video_table").DataTable().destroy()
        list_all_video_table = $('#list_all_video_table').DataTable({
            "order": [[1, "desc"]], // for descending order
            "columnDefs": [
                { "width": "50%", "targets": 0 }
            ]
        })

        // add refresh button to data table
        $('<button class="btn btn-secondary btn-xs waves-effect table-refresh-btn mr-1" id="list-all-assetstable-refresh-btn"> <i class="mdi mdi-refresh mr-1"></i> <span>Refresh</span> </button>').prependTo("#list_all_video_table_filter");

        resolve()
    })
}


// change Asset Thumbnail
function changeAssetThumbnail(content_id, thumbnail_pic) {
    var poster = "https://68tifeufag8ntkaiul6tm6jigd-transcoded.s3.ap-south-1.amazonaws.com/transcode/processed-data/" + content_id + "/thumbnails/" + thumbnail_pic + "";
    player.poster(poster);

    if (assetPreviewPlayer) {
        // change thumbnail of preview player also
        assetPreviewPlayer.poster(poster);
    }
}


function initiateAssetSelection() {
    $("#list_all_video_table tbody tr:first").addClass("active");
    let preview_url = $("#list_all_video_table tbody tr:first").children('.action-td').find('.preview-video').attr("data-preview_url");

    let thumbnail_pic = $("#list_all_video_table tbody tr:first").children('.action-td').find('.preview-video').attr("data-thumbnail_pic");
    let content_id = $("#list_all_video_table tbody tr:first").attr('id')


    // initiate player
    initiatePlayer('asset-player', preview_url).then(() => {
        // initiate poster
        changeAssetThumbnail(content_id, thumbnail_pic)
    });

}

// getLink copy url
function copy() {

    var copyText = document.getElementById("getlinkurl");
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");

}

// Preview Video Asset
$(document).on("click", ".preview-video", function () {

    var asset_title = $(this).parent().parent().siblings('.asset-title').text();
    var content_id = $(this).parent().parent().attr('id')
    var preview_url = $(this).attr("data-preview_url")
    var thumbnail_pic = $(this).attr("data-thumbnail_pic")

 
    $('.preview-modal-asset-title').html(asset_title);

    // initiate player
    initiateAssetPreviewPlayer('preview-asset-player', preview_url).then(() => {
        // initiate poster
        changeAssetThumbnail(content_id, thumbnail_pic)
    })
})

// getLink Preview Video Asset Overview
$(document).on("click", ".getlink-preview", function () {

    var Cloudurl = $(this).attr('cloudFront_video');

    $('#getlinkurl').val(Cloudurl);
    $('#getlink').modal('show');
})


function _formateThumbnails(asset_data) {
    return new Promise((resolve, reject) => {

        // base url for thumbnails
        var preview_url = (asset_data.preview_url).split('/');
        var protocol = preview_url[0];
        var host = preview_url[2];
        var base_url = `${protocol}//${host}`

        var thumbnailArr = asset_data.thumbnail;
        const thumbnailArrLength = ((thumbnailArr.length > 12) ? 12 : (thumbnailArr.length));

        var options = '<div class="container thumbnails-container mt-1" style="display:none"> <div class="row">';

        for (var i = 0; i <= thumbnailArrLength; i++) {

            // for active thumbnail
            if (thumbnailArr[i] === asset_data.thumbnail_pic) {
                options += '<div class="col-md-2"><img class="img-fluid thumb-img active" src="' + base_url + '/transcode/processed-data/' + asset_data.content_id + '/thumbnails/' + thumbnailArr[i] + '" ></div>';

                // set thumbnail to Asset
                changeAssetThumbnail(asset_data.content_id, thumbnailArr[i])

            } else {
                options += '<div class="col-md-2"><img class="img-fluid thumb-img" src="' + base_url + '/transcode/processed-data/' + asset_data.content_id + '/thumbnails/' + thumbnailArr[i] + '" ></div>'
            }

            if (i === (thumbnailArrLength - 1)) {
                options += `</div><div class="row"><div class="col-md-12 mt-1"><button id="set-thumbnail-btn" type="button" class="btn btn-xs btn-purple waves-effect waves-light change-thumbnail mt-1" data-asset-id=${asset_data.content_id}>Change Thumbnail</button></div></div></div>`
                resolve(options)
            }
        }
    });
}



function formatAssetDetails(asset_data) {

    return new Promise((resolve, reject) => {

        let profiles = asset_data.profiles;
        let file_name = asset_data.file_name;
        let formatedAssetDetails = '';
        let formateThumbnails = '';

        let basic_asset, standard_asset, premium_asset, custom_asset = '';

        if (profiles.basic.status === 'COMPLETE') {
            basic_asset = `<li class="list-group-item transcoded-profile" data-url=${profiles.basic.url}><i class="mdi mdi-video"></i>${file_name}_BASIC.m3u8</li>`;
            formatedAssetDetails += basic_asset;
        } else {
            basic_asset = ''
        }
        if (profiles.standard.status === 'COMPLETE') {
            standard_asset = `<li class="list-group-item transcoded-profile" data-url=${profiles.standard.url}><i class="mdi mdi-video"></i>${file_name}_STANDARD.m3u8</li>`;
            formatedAssetDetails += standard_asset;
        } else {
            standard_asset = ''
        }
        if (profiles.premium.status === 'COMPLETE') {
            premium_asset = `<li class="list-group-item transcoded-profile" data-url=${profiles.premium.url}><i class="mdi mdi-video"></i>${file_name}_PREMIUM.m3u8</li>`;
            formatedAssetDetails += premium_asset;
        } else {
            premium_asset = ''
        }
        if (profiles.custom.status === 'COMPLETE') {
            custom_asset = `<li class="list-group-item transcoded-profile" data-url=${encodeURIComponent(profiles.custom.url)}><i class="mdi mdi-video"></i>${file_name}_CUSTOM.m3u8</li>`;
            formatedAssetDetails += custom_asset;
        } else {
            custom_asset = ''
        }

        // thumbnails
        if (asset_data.media_job_status === 'COMPLETE') {
            _formateThumbnails(asset_data).then((thumbnails) => {
                formateThumbnails = thumbnails;
                formatedAssetDetails += formateThumbnails;

                resolve(`<ul class="list-group list-group-flush">${basic_asset}${standard_asset}${premium_asset}${custom_asset}<li class="list-group-item thumbnails-folder"><i class="mdi mdi-folder"></i>Thumbnails</li></ul>${formateThumbnails}`)
            })
        } else {
            resolve('<div class="asset-details"></div>')
        }

    })
}


// change thumbnail
$(document).on("click", ".change-thumbnail", function (e) {

    var thumbnail_pic = $(".thumb-img.active").attr('src');

    let params = {
        content_id: $(this).attr("data-asset-id"),
        thumbnail_pic: thumbnail_pic.split("/").pop()
    }

    $.post('/v1/video/metadatasave',
        params,
        function (data, status) {
            if (data.status === 200) {

                toastr.success('Thumbnail Updated.');
                changeAssetThumbnail(params.content_id, params.thumbnail_pic)

            } else if (data.status == 400) {
                toastr.danger('Thumbnail update failed.')
            }
        });
})



// Transcoded Asset Selection
$(document).on("click", ".transcoded-profile", function (e) {

    let transcoded_url = decodeURIComponent($(this).attr("data-url"));
    let content_id = $(this).closest('tr').prev().attr('id');
    let thumbnail_pic = $(this).closest('tr').prev().data("thumbnail_pic");

    // initiate player
    initiatePlayer('asset-player', transcoded_url).then(() => {
        // initiate poster
        changeAssetThumbnail(content_id, thumbnail_pic)
    });

});


// Asset Package Selection
$(document).on("click", ".asset-overview-tbl-row", function (e) {

    $('.asset-overview-tbl-row').removeClass("active");
    $(this).addClass("active");


    // ########## child rows code ########### //
    var tr = $(this).closest('tr');
    var row = list_all_video_table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');

    } else {

        let preview_url = $(this).children('.action-td').find('.preview-video').attr("data-preview_url");
        let asset_id = $(".asset-overview-tbl-row.active").attr('id');

        //preload metadata
        getMetaData(asset_id).then((asset_data) => {

            // initiate player
            initiatePlayer('asset-player', preview_url)

            // to close the child of row which is already open
            if (list_all_video_table.row('.shown').length) {
                var _tr = $('tr.shown');
                var _row = list_all_video_table.row(_tr);
                _row.child.hide();
                _tr.removeClass('shown');
            }

            formatAssetDetails(asset_data).then((assetDetails) => {
                // Open this row
                row.child(assetDetails).show();
                tr.addClass('shown');
            })

        })
    }
});


function getMetaData(content_id) {

    return new Promise((resolve, reject) => {
        $.post('/v1/video/preload_metadata',
            {
                content_id: content_id,
            },
            function (data, status) {
                if (data.status === 200) {

                    //console.log(JSON.stringify(data))

                    //clear the form
                    $('#asset-metadata-form').trigger("reset");
                    $("#cast").tagsinput('removeAll');
                    $("#keywords").tagsinput('removeAll');

                    let _data = data.data;

                    $('#title').val(_data.title)
                    $('#cast').tagsinput('add', _data.cast);
                    $('#description').val(_data.description)
                    $('#keywords').tagsinput('add', _data.keywords);
                    $('#language').val(_data.language)

                    if (_data.is_featured) {
                        $('#is_featured').prop('checked', true);
                    } else {
                        $('#is_popular').prop('checked', true);
                    }

                    resolve(_data)

                } else {
                    toastr.error(data.message);
                    reject()
                }
            })

    });

}


function transcodeAsset_default(params) {
   
    $.post('/v1/video/transcode_asset',
        params,
        function (data, status) {
            if (data.status === 200) {
                toastr.success(data.message);
            } else {
                toastr.error(data.message);
            }
        })
}


function transcodeAsset_custom(params) {

    $.post('/v1/video/transcode_custom_asset',
        params,
        function (data, status) {
            if (data.status === 200) {
                toastr.success(data.message);
            } else {
                toastr.error(data.message);
            }
        })
}


function getCloudFront1() {
    return new Promise((resolve, reject) => {
        $.get('/v1/video/getCloudFront',
            
            function (data, status) {
                if (data.status === 200) {
                    resolve(data)
                 
                } else {
                    toastr.error(data.message);
                }
            })
        })
    }
    

// Transcode Asset
$(document).on("click", "#transcode-asset-btn", function (e) {

  
    var selected_profiles = $("#transcode-profiles").val();
    var default_profiles = [];
    var custom_profiles = [];

    selected_profiles.forEach((element, i) => {

        if (selected_profiles[i] === 'basic' || selected_profiles[i] === 'standard' || selected_profiles[i] === 'premium') {
            default_profiles.push(selected_profiles[i]);
        } else {
            custom_profiles.push(selected_profiles[i]);
        }

    })


        if (default_profiles.length >= 1) {
            var params = {
                content_id: $(".asset-overview-tbl-row.active").attr('id'),
                content_path: $(".asset-overview-tbl-row.active").children('.action-td').find('.preview-video').attr("data-video_path"),
               
            }
           
            params.profiles = default_profiles;
            transcodeAsset_default(params)
        }

        if (custom_profiles.length >= 1) {
            var _params = {
                content_id: $(".asset-overview-tbl-row.active").attr('id'),
                content_path: $(".asset-overview-tbl-row.active").children('.action-td').find('.preview-video').attr("data-video_path"),
                
            }
            _params.profiles = custom_profiles;
            transcodeAsset_custom(_params)
        }
   
    e.preventDefault();
})




// Delete Video Aseet
$(document).on("click", ".delete-video", function (e) {

    var content_id = $(this).parents(".asset-row").attr('id');

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {
            $.ajax({
                url: `/v1/video/deletevideo/${content_id}`,
                type: 'DELETE',
                headers: {
                    'Content-Type': "application/json",
                },
                'success': function (data, textStatus, request) {
                    if (data.status === 200) {
                        toastr.success('Asset deleted Successfully.');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error('Asset delete failed');
                    }
                },
                'error': function (request, textStatus, errorThrown) {
                    console.log("ERR " + errorThrown)
                }

            })
        }
    })
    e.preventDefault()
})


// Metadata Saved
$(document).on("click", "#edit-asset-btn", function () {
    //console.log(JSON.stringify(dynamicInputGroupsIDs_arr))

    var params = {
        content_id: $(".asset-overview-tbl-row.active").attr('id') ? $(".asset-overview-tbl-row.active").attr('id') : null,
        title: $('#title').val() ? $('#title').val().trim() : null,
        keywords: $('#keywords').val() ? $('#keywords').val() : null,
        language: $('#language').val() ? $('#language').val().trim() : null,
        description: $('#description').val() ? $('#description').val().trim() : null,
        cast: $('#cast').val() ? $('#cast').val() : null,
        is_featured: $("#is_featured").is(':checked') ? $("#is_featured").is(':checked') : false,
        is_popular: $("#is_popular").is(':checked') ? $("#is_popular").is(':checked') : false,
        dynamicInputGroupsIDs_arr: (dynamicInputGroupsIDs_arr !== []) ? dynamicInputGroupsIDs_arr : null
    }

    for (var i = 0; i < dynamicInputGroupsIDs_arr.length; i++) {

        let thisType = dynamicInputGroupsIDs_arr[i].type;
        let thisId = dynamicInputGroupsIDs_arr[i].id;

        if (thisType == 'text') {
            params[thisId] = $(`#${thisId}`).val();
        }
        if (thisType == 'checkbox') {
            params[thisId] = $(`#${thisId}`).is(':checked');
        }
        if (thisType == 'radio') {
            params[thisId] = $(`#${thisId}`).is(':checked');

        }
        if (thisType == 'textarea') {
            params[thisId] = $(`#${thisId}`).val();

        }
    }

    console.log("dynamicInputGroupsIDs_arr params >> " + JSON.stringify(params))

    //console.log("av "+JSON.stringify(params))

    $.post('/v1/video/metadatasave',
        params,
        function (data, status) {
            if (data.status === 200) {

                toastr.success('Asset Updated.');

            } else if (data.status == 400) {
                toastr.danger('Asset update failed.')
            }
        });
})


// thumbnail folder click
$(document).on("click", ".thumbnails-folder", function () {
    $(".thumbnails-container").toggle(300);
})


// thumbnail-click
$(document).on("click", ".thumb-img", function () {
    $('.thumb-img').removeClass("active");
    $(this).addClass("active");
})
