var express = require('express');
var router = express.Router();
const AWS = require('aws-sdk');

const Profiles = require("../workers/profiles");
const profilesObj = new Profiles.ProfilesClass();

const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

const mediaConvertUtil = require('../app/utilities/video/media_convert');



// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');


// ########## MICRO-1 ##########

let mcube_transcode_BASE = API.mcube_transcode.base.development;


router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('profile/create_profile', { section: 'Profile', sub_section: 'Create Profile', userData: userData, userName: userName, userRole: userRole });
});


router.get('/profile_overview', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('profile/profile_overview', { section: 'Profile Overview', sub_section: '', userData: userData, userName: userName, userRole: userRole });
});


/* POST : SAVE Profiles DETAILS. */
router.post('/', (req, res, next) => {
    console.log("\n create profile called in mcube_ui")

    const tenantId = req.session.tenantId;
    const mediaParams = req.body.profiles ? req.body.profiles : null;

    mediaConvertUtil.commonJSON().then((unformattedMediaJSON) => {
        mediaConvertUtil.forMatMediaJSONByPublisher(unformattedMediaJSON, mediaParams).then((mediaJSON) => {

            const params = {
                profile_name: req.body.profile_name ? req.body.profile_name : null,
                profiles: req.body.profiles ? req.body.profiles : null,
                mediaJSON: mediaJSON ? mediaJSON : null,
                mongoDbName: tenantConfig[tenantId].config.mongodb
            }

            console.log("FINAL MEDIA JSON >> "+ JSON.stringify(params))

            profilesObj.save(params).then((_response) => {
                if (_response) {
                    res.json({
                        status: 200,
                        message: 'Profiles Saved successfuly',
                        data: _response,
                    })

                } else {
                    res.json({
                        status: 401,
                        message: 'Profiles Save failed',
                        data: null,
                    })
                }
            })

        })
    })

});


// Get All Profiles
router.get('/getAllProfiles', (req, res, next) => {

    console.log('\n getAllProfiles called in mcube_ui')
    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.profile.getAllProfiles,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.getApiRequestWithHeaders(params).then((profiles) => {
        res.json(profiles);
    });

});



// #############################################################################

//delete profile
router.delete('/deleteProfile/:_id', function (req, res, next) {

    console.log('*** delete profile CALLED***')
    var params = {
        url: mcube_transcode_BASE + API.mcube_transcode.profile.deleteProfile + req.params._id,
        data: {},
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.deleteApiRequestWithHeader(params).then((response) => {
        res.json(response);
    });
});


//##########################################################################


// preload profile data
router.post('/preload_profiledata', (req, res, next) => {

    console.log('*** preload Profiledata CALLED***')

    var params = {
        url: mcube_transcode_BASE + API.mcube_transcode.profile.preload_profiledata,
        data: {
            profile_name: req.body.profile_name
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });

});

module.exports = router;