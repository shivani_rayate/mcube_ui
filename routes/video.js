const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const videoPartial = require('../app/utilities/video/index');
const mediaConvertUtil = require('../app/utilities/video/media_convert');

const uuidv1 = require('uuid/v1');

const Video = require('../workers/video');
const videoObj = new Video.VideoClass();

const Profiles = require("../workers/profiles");
const profilesObj = new Profiles.ProfilesClass();

const video_utility = require('../app/utilities/video');
const listS3 = require('../app/utilities/listS3');
const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const s3 = new AWS.S3();

// ##############################################################################  

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/api_list');

// ########## MICRO-1 ##########
let mcube_transcode_BASE = API.mcube_transcode.base.development;

// ########## MICRO-2 ##########

let mcube_adv_transcode_BASE = API.mcube_adv_transcode.base.development;

router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    const cloudFront = req.session.cloudFront ? req.session.cloudFront : null;
    res.render('asset_management/upload_content', { section: 'Asset Management', sub_section: 'Upload Content', userData: userData, userName: userName, userRole: userRole, cloudFront: cloudFront });
});

router.get('/asset-overview', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    const cloudFront = req.session.cloudFront ? req.session.cloudFront : null;
    res.render('asset_management/asset_overview', { section: 'Asset Management', sub_section: 'Asset Overview', userData: userData, userName: userName, userRole: userRole, cloudFront: cloudFront });
});



// Asset Upload
router.post('/', (req, res, next) => {

    console.log("\n asset upload route called in mcube_ui")

    var CloudFront = req.session.cloudFront;
    console.log("\n \n res >> " + CloudFront)

    const _contentId = uuidv1();
    const tenantId = req.session.tenantId;
    const bucket = tenantConfig[tenantId].config.config.s3BucketList.inputBucket;

    var fileName = '';
    const upload = multer({
        storage: multerS3({
            s3: s3,
            acl: 'public-read',
            bucket: `${bucket}/${awsConfig.transcode_module}/${awsConfig.putfolder}/${_contentId}`,
            key: (req, file, cb) => {
                console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
                fileName = `${_contentId}${awsConfig.video.extension}`;
                console.log(`FILE NAME>> ${fileName}`);
                cb(null, fileName);
            },
        }),
    }).array('videoUpload', 1);

    upload(req, res, (error) => {
        if (error) {
            console.log(`VIDEO UPLOAD ERROR: ${error}`);
        } else {
            console.log('VIDEO UPLOAD DONE!!!!!!');
            var CloudFront_URL = req.session.cloudFront;
            console.log("\n \n CloudFront_URL >> " + CloudFront_URL)

            const bodyParams = {
                file_name: req.body.file_name ? req.body.file_name : null,
                content_id: _contentId,
                mongoDbName: tenantConfig[tenantId].config.mongodb
            }

            videoObj.save(bodyParams).then((videoData) => {
                res.json({
                    status: 200,
                    data: videoData,
                    message: 'Video saved successfully!',
                });

                let _params = {
                    mongoDbName: tenantConfig[tenantId].config.mongodb,
                    tableName: tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.previewTbl,
                    contentId: _contentId,
                    transcode_type: 'preview',
                    bucket: tenantConfig[tenantId].config.config.s3BucketList.outputBucket,
                    CloudFront_URL: CloudFront_URL
                };

                // get preview status from dynamodb and update into mongoDB
                video_utility.getDynamodbStatus(_params);
            })
        }
    });

})

// get all assets 
router.post('/getDatafromDyanamoDb', (req, res, next) => {
    console.log('\n /getDatafromDyanamoDb ');
    let tenantId = req.session.tenantId;
    let tenantName = tenantConfig[tenantId].tenantName;
    let startDate = req.body.startDate;


    let newDate = new Date(startDate).toDateString()
    var arr1 = newDate.split(' ');

    var month = arr1[1]
    var year = arr1[3]

    var clientName = `${tenantName}_${month}-${year}`

    console.log('\n /getDatafromDyanamoDb ' + clientName);

    var _params = {
        clientName: clientName
    };


    listS3.getItemIntoDyanamodb(_params).then((data) => {
        console.log(" \n getItemIntoDyanamodb >> " + JSON.stringify(data));
        if (data) {

            res.json({
                status: 200,
                message: 'list user pool Successfully',
                data: data,
            })
            // successful response
        } else {
            console.log(err)
        }
    });



});

// Custom Thumbnail Upload
router.post('/upload-thumbnail', (req, res, next) => {

    console.log("\n upload-thumbnail route called in mcube_ui")

    const bodyParams = {
        thumbnail: `${req.query.thumbnail}${awsConfig.image.extension}`,
        content_id: req.query.content_id,
    };

    let tenantId = req.session.tenantId;
    let bucket = tenantConfig[tenantId].config.config.s3BucketList.outputBucket;

    //console.log(`S3 PATH>> ${bucket}/${awsConfig.transcode_module}/${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${bodyParams.content_id}/${awsConfig.thumbnail_folder}`);
    const upload = multer({
        storage: multerS3({
            s3: s3,
            acl: 'public-read',
            bucket: `${bucket}/${awsConfig.transcode_module}/${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${bodyParams.content_id}/${awsConfig.thumbnail_folder}`,
            key: (req, file, cb) => {
                console.log(`REAL FILENAME>> ${JSON.stringify(file)}`);
                console.log(`bodyParams.thumbnail_name>> ${bodyParams.thumbnail}`);
                cb(null, bodyParams.thumbnail);
            },
        }),
    }).array('thumnailUpload', 1);

    // eslint-disable-next-line func-names
    // eslint-disable-next-line prefer-arrow-callback
    upload(req, res, (error) => {
        console.log(`UPLOAD ERROR: ${error}`);
        if (error) {
            console.log(`UPLOAD ERROR: ${error}`);
        } else {
            console.log('UPLOAD DONE!!!!!!');
            console.log(`FILE UPLOAD COMPLETE11!!>>> ${JSON.stringify(bodyParams)}`);
            videoObj.findOneAndUpdate(bodyParams).then((_response) => {
                if (_response) {
                    console.log('Video updated successfully');
                    res.json({
                        status: 200,
                        data: _response,
                        message: 'Video updated successfully',
                    });
                } else {
                    res.json({
                        status: 500,
                        message: 'Oops looks like something went wrong, please try again later',
                        data: null,
                    });
                }
            });
        }
    });
});


// get all assets 
router.get('/fetch_all_videos', (req, res, next) => {
    console.log('\n /fetch_all_videos route called in mcube_ui');
    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.list,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.getApiRequestWithHeaders(params).then((videos) => {
        res.json(videos);
    });
});


// get todays uploaded asset
router.get('/todays_uploaded', (req, res, next) => {
    console.log('\n /todays_uploaded route called in mcube_ui');
    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.todays_uploaded,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.getApiRequestWithHeaders(params).then((count) => {
        res.json(count);
    });
});


// Transcode-asset
router.post('/transcode_asset', function (req, res, next) {

    console.log('\n /transcode_asset route called in mcube_ui')

    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.transcode_asset,
        data: {
            content_id: req.body.content_id,
            profiles: req.body.profiles,
            content_path: req.body.content_path,
            CloudFront: req.session.cloudFront
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    };
    requestMethods.postApiRequestWithHeader(params).then((response) => {
        res.json(response);
    });
})

// Get cloudFront END POINT
router.get('/getCloudFront', function (req, res, next) {

    console.log('\n /getCloudFront route called in mcube_ui')
    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.getCloudFront,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.getApiRequestWithHeaders(params).then((data) => {
        res.json(data);
    });
})


// Transcode-custom-asset
router.post('/transcode_custom_asset', function (req, res, next) {
    console.log('\n /transcode_custom_asset route called in mcube_ui')

    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.transcode_custom_asset,
        data: {
            content_id: req.body.content_id,
            content_path: req.body.content_path,
            profiles: req.body.profiles,
            CloudFront: req.session.cloudFront
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    };
    requestMethods.postApiRequestWithHeader(params).then((videoData) => {
        res.json(videoData);
    });
})


//delete asset
router.delete('/deletevideo/:content_id', function (req, res, next) {

    console.log('\n /deletevideo route called in mcube_ui')

    var params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.deletevideo + req.params.content_id,
        data: {},
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.deleteApiRequestWithHeader(params).then((response) => {
        res.json(response);
    });

});


// save video metadata
router.post('/metadatasave', (req, res, next) => {

    // console.log("checking for array here >>> "+ JSON.stringify(req.body))

    const params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.metadatasave,
        data: req.body,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.postApiRequestWithHeader(params).then((response) => {
        res.json(response);
    });

})

// preload metadata Asset
router.post('/preload_metadata', (req, res, next) => {


    const params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.preload_metadata,
        data: {
            content_id: req.body.content_id
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }

    };
    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    })

});


// get asset status
router.post('/get_asset_status', (req, res, next) => {


    const params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.get_asset_status,
        data: {
            content_id: req.body.content_id
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    };
    //console.log('***GET ASSET STATUS PARAMS ***' + JSON.stringify(params));
    requestMethods.postApiRequestWithHeader(params).then((videos) => {
        res.json(videos);
    });
});



// ************************************************************************ //
// ###################### --- Advanced Transcode --- ###################### //




router.get('/file-overview', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('asset_management/file_overview', { section: 'Asset Management', sub_section: 'File Overview', userData: userData, userName: userName, userRole: userRole });
});


router.get('/advance-transcoding', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('asset_management/advance_transcoding', { section: 'Asset Management', sub_section: 'Advance Transcoding', userData: userData, userName: userName, userRole: userRole });
});


router.get('/listS3_Assets', function (req, res, next) {

    console.log('***GET ALL listS3_Assets CALLED in UI***');

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.listS3_Assets,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.getApiRequestWithHeaders(params).then((videos) => {
        res.json(videos);
    });

})

router.post('/listS3_AssetsList', function (req, res, next) {

    console.log('***GET ALL listS3_Assets invoice CALLED in UI***');

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.listS3_Assets,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.getApiRequestWithHeaders(params).then((videos) => {
        res.json(videos);
    });

})

// create preview of S3 List Asset
router.post('/create-preview', (req, res, next) => {

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.createPreview,
        data: {
            content_id: req.body.content_id,
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    };

    requestMethods.postApiRequestWithHeader(params).then((response) => {
        res.json(response);
    });

});


// getlink preview asset
router.post('/getlink-preview-asset', (req, res, next) => {
    console.log("getlink preview POST CALLED in UI")
    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.getlinkPreview,
        data: req.body,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    console.log(" \n getlink preview POST CALLED in UI " + JSON.stringify(params))
    requestMethods.postApiRequestWithHeader(params).then((url) => {
        res.json(url)
    })

});

// getlink preview asset OverView
router.post('/getlink-preview', (req, res, next) => {
    console.log("getlink preview asset OverView POST CALLED in UI")
    let params = {
        url: mcube_transcode_BASE + API.mcube_transcode.video.getlinkPreview,
        data: req.body,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.postApiRequestWithHeader(params).then((url) => {
        res.json(url)
    })

});


router.get('/listS3_PreviewAssets', function (req, res, next) {
    console.log('*** GET ALL listS3_PreviewAssets CALLED in UI ***');

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.listS3_PreviewAssets,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.getApiRequestWithHeaders(params).then((videos) => {
        res.json(videos);
    });

})


// meta data
router.post('/listS3ContentMetadatatDynamodb', function (req, res, next) {

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.listS3ContentMetadatatDynamodb,
        data: {
            asset_id: req.body.asset_id,
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });

})


// upload audio files to S3
router.post('/uploadAudio/:params', (req, res, next) => {

    console.log("audio upload route called");

    // get Tenant Config from Session
    var tenantId = req.session.tenantId;
    var bucket = tenantConfig[tenantId].config.config.s3BucketList.inputBucket;

    var _params = JSON.parse(req.params.params)

    let asset_id = _params.asset_id;
    let language = _params.language;

    // for uploading audio files
    var audioFileNameArr = [];
    var _i = 0;

    var upload = multer({

        storage: multerS3({

            s3: s3,
            bucket: `${bucket}/${awsConfig.adv_transcode_module}/${awsConfig.putfolder}/${asset_id}`,
            acl: 'public-read',
            contentType: multerS3.AUTO_CONTENT_TYPE,

            key: function (req, file, cb) {

                //console.log(file);

                var extension = (file.originalname).split('.').pop();
                var audioFileName = `${asset_id}_${language[_i]}.${extension}`;
                if (file.fieldname == 'files') {
                    audioFileNameArr.push({
                        "langauge": language[_i],
                        "fileName": audioFileName
                    })
                }
                cb(null, audioFileName);
                _i++;
            }
        })

    }).fields([{
        name: 'files', maxCount: 10
    }]);
    // audio file upload ends here

    upload(req, res, (error) => {
        if (error) {
            console.log(`AUDIO UPLOAD ERROR: ${error}`);
        } else {
            console.log("upload audio success")
            //console.log("audioFileNameArr " + JSON.stringify(audioFileNameArr))
            res.json({
                status: 200,
                data: audioFileNameArr,
                message: 'Audio Files Uploaded Successfully!',
            });


            // to update dynamodb
            let _params = {
                assetid: asset_id,
                audio_info: audioFileNameArr,
                TableName: tenantConfig[tenantId].config.config.dynamodbTables.mcube_adv_transcode.advtranscodeDbName
            };
            listS3.updateDynamodb(_params);

        }
    })

})


// transcode-asset-disney
router.post('/transcode-asset-disney', function (req, res, next) {
    console.log("transcode-asset-disney route called in UI");

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.transcodeAssetDisney,
        data: req.body,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }
    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });
})


// create meta data AZURE
router.post('/create-meta-data-azure', function (req, res, next) {

    console.log("create-meta-data-azure route called");

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.createMetaDataAzure,
        data: req.body,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }

    }

    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });
})


// get meta data AZURE
router.post('/get-meta-data-azure', function (req, res, next) {

    console.log("get-meta-data-azure route called in UI");

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.getMetaDataAzure,
        data: req.body,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });
})


// Get meta data Imdb
//const imdb = require('../app/utilities/Metadata/imdb');
router.post('/getImdbData', function (req, res, next) {

    console.log("getImdbData route called in UI");

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.getImdbData,
        data: {
            asset_id: req.body.asset_id,
            name: req.body.name,
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });

})


//update-IMD metadata status into dynamoDB
router.post('/updateImdbStatus', function (req, res, next) {

    console.log("updateImdbStatus route called in UI");

    let params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.updateImdbStatus,
        data: {
            asset_id: req.body.asset_id,
            imdb_data_status: req.body.imdb_data_status,
        },
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    }

    requestMethods.postApiRequestWithHeader(params).then((data) => {
        res.json(data);
    });

})


// find Imdb data by Id
router.get('/findMetaData-imdb/:asset_id', function (req, res, next) {
    console.log("findMetaData-imdb route called");

    const params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.findMetaDataImdb + req.params.asset_id,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    };
    requestMethods.getApiRequestWithHeaders(params).then((metaData) => {
        res.json(metaData);
    })
})


// CHECK MEDIA CONVERTER STATUS WITH JOBID
router.get('/getMediaConvertStatus/:job_id', function (req, res, next) {
    console.log("getMediaConvertStatus route called in UI");

    const params = {
        url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.video.getMediaConvertStatus + req.params.job_id,
        headers: {
            token: req.session.userData.idToken.jwtToken
        }
    };
    requestMethods.getApiRequestWithHeaders(params).then((metaData) => {
        res.json(metaData);
    })
})





module.exports = router;